export const headerText = {
  home: 'Home',
  about: 'About',
  services: 'Services',
  servicesHousehold: 'Daily Household Tasks',
  servicesDailyLiving: 'Daily Living',
  servicesCommunity: 'Community Participation',
  servicesCommunityNursing: 'Community Nursing',
  servicesAccommodation: 'Accommodation',
  contact: 'Contact'
}
