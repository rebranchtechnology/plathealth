export const pageRoutes = {
  home: '/',
  about: '/about',
  services: '/services',
  servicesHousehold: '/services/householdTasks',
  servicesDailyLiving: '/services/dailyLiving',
  servicesCommunity: '/services/communityParticipation',
  servicesCommunityNursing: '/services/communityNursing',
  servicesAccommodation: '/services/accommodation',
  contact: '/contact'
}

export const pageRoutesPriorities = {
  home: '1.0',
  about: '1.0',
  services: '1.0',
  servicesHousehold: '0.8',
  servicesDailyLiving: '1.0',
  servicesCommunity: '0.8',
  servicesCommunityNursing: '0.8',
  servicesAccommodation: '1',
  contact: '1.0'
}
