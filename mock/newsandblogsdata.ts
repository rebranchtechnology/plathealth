import BannerImage1 from '@assets/images/media/banner.png'
import BlogBanner1 from '@assets/images/media/blog-banner.png'
import NewsBanner1 from '@assets/images/media/news-banner.png'
import BlogContentImage1 from '@assets/images/media/blog-content.png'
import NewsContentImage1 from '@assets/images/media/news-content.png'
import CommentorImage1 from '@assets/images/media/commentor1.png'
import { CommentsSectionProps } from '@components/Media/CommentsSection'
export interface detailsObject {
  id: number
  comments: CommentsSectionProps[]
  title: string
  pageBanner: StaticImageData
  contentBanner: StaticImageData
  contentImage: StaticImageData
  category: string
  postedAt: string
  para1: string
  para2: string
  para3: string
}

export const newsDetails:detailsObject[] = [
  {
    id: 1,
    comments: [
      {
        userName: 'Alex Morgan',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Emili Fox',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Piers Morgan',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Tucker Carlson',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Sean Hannity',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Ingraham Angle',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'John Doe',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      }
    ],
    title: 'Lorem ipsum dolor sit amet, consecteur adipiscing elit.',
    pageBanner: BannerImage1,
    contentBanner: NewsBanner1,
    category: 'Health',
    postedAt: "March 12, 2020",
    para1:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor cursus pulvinar mauris posuere tellus nisi, velit massa integer. Netus urna morbi eget sit duis ante aliquet duis. Faucibus est feugiat ut auctor sapien, et. Eget tortor tortor, non morbi et odio. Lectus sed in rhoncus euismod eros, velit convallis ornare est. Eu amet cursus ut eros. Elementum sed etiam arcu etiam id. A et sit nulla varius turpis commodo viverra augue. Pulvinar magna nulla dignissim a. Eu tristique odio ultrices euismod nunc semper neque. Interdum egestas consequat habitasse lectus nam eget diam. Et cras at aliquet feugiat ut varius. Senectus quam ante enim orci vitae sit tempor iaculis feugiat. Morbi at mi etiam aliquet eget urna scelerisque. Risus sed est augue convallis posuere egestas ridiculus nunc, pellentesque.',
    para2:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor cursus pulvinar mauris posuere tellus nisi, velit massa integer. Netus urna morbi eget sit duis ante aliquet duis. Faucibus est feugiat ut auctor sapien, et. Eget tortor tortor, non morbi et odio. Lectus sed in rhoncus euismod eros, velit convallis ornare est. Eu amet cursus ut eros. Elementum sed etiam arcu etiam id. A et sit nulla varius turpis commodo viverra augue. Pulvinar magna nulla dignissim a. Eu tristique odio ultrices euismod nunc semper neque. Interdum egestas consequat habitasse lectus nam eget diam. Et cras at aliquet feugiat ut varius. Senectus quam ante enim orci vitae sit tempor iaculis feugiat. Morbi at mi etiam aliquet eget urna scelerisque. Risus sed est augue convallis posuere egestas ridiculus nunc, pellentesque.',
    para3:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor cursus pulvinar mauris posuere tellus nisi, velit massa integer. Netus urna morbi eget sit duis ante aliquet duis. Faucibus est feugiat ut auctor sapien, et. Eget tortor tortor, non morbi et odio. Lectus sed in rhoncus euismod eros, velit convallis ornare est. Eu amet cursus ut eros.',
    contentImage: NewsContentImage1
  },
  {
    id: 2,
    comments: [
      {
        userName: 'Alex Morgan',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Emili Fox',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Piers Morgan',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Tucker Carlson',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Sean Hannity',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Ingraham Angle',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'John Doe',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      }
    ],
    title: 'Lorem ipsum dolor sit amet, consecteur adipiscing elit.',
    pageBanner: BannerImage1,
    contentBanner: NewsBanner1,
    category: 'Health',
    postedAt: "March 12, 2020",
    para1:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor cursus pulvinar mauris posuere tellus nisi, velit massa integer. Netus urna morbi eget sit duis ante aliquet duis. Faucibus est feugiat ut auctor sapien, et. Eget tortor tortor, non morbi et odio. Lectus sed in rhoncus euismod eros, velit convallis ornare est. Eu amet cursus ut eros. Elementum sed etiam arcu etiam id. A et sit nulla varius turpis commodo viverra augue. Pulvinar magna nulla dignissim a. Eu tristique odio ultrices euismod nunc semper neque. Interdum egestas consequat habitasse lectus nam eget diam. Et cras at aliquet feugiat ut varius. Senectus quam ante enim orci vitae sit tempor iaculis feugiat. Morbi at mi etiam aliquet eget urna scelerisque. Risus sed est augue convallis posuere egestas ridiculus nunc, pellentesque.',
    para2:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor cursus pulvinar mauris posuere tellus nisi, velit massa integer. Netus urna morbi eget sit duis ante aliquet duis. Faucibus est feugiat ut auctor sapien, et. Eget tortor tortor, non morbi et odio. Lectus sed in rhoncus euismod eros, velit convallis ornare est. Eu amet cursus ut eros. Elementum sed etiam arcu etiam id. A et sit nulla varius turpis commodo viverra augue. Pulvinar magna nulla dignissim a. Eu tristique odio ultrices euismod nunc semper neque. Interdum egestas consequat habitasse lectus nam eget diam. Et cras at aliquet feugiat ut varius. Senectus quam ante enim orci vitae sit tempor iaculis feugiat. Morbi at mi etiam aliquet eget urna scelerisque. Risus sed est augue convallis posuere egestas ridiculus nunc, pellentesque.',
    para3:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor cursus pulvinar mauris posuere tellus nisi, velit massa integer. Netus urna morbi eget sit duis ante aliquet duis. Faucibus est feugiat ut auctor sapien, et. Eget tortor tortor, non morbi et odio. Lectus sed in rhoncus euismod eros, velit convallis ornare est. Eu amet cursus ut eros.',
    contentImage: NewsContentImage1
  },
  {
    id: 3,
    comments: [
      {
        userName: 'Alex Morgan',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Emili Fox',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Piers Morgan',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Tucker Carlson',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Sean Hannity',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Ingraham Angle',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'John Doe',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      }
    ],
    title: 'Lorem ipsum dolor sit amet, consecteur adipiscing elit.',
    pageBanner: BannerImage1,
    contentBanner: NewsBanner1,
    category: 'Health',
    postedAt: "March 12, 2020",
    para1:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor cursus pulvinar mauris posuere tellus nisi, velit massa integer. Netus urna morbi eget sit duis ante aliquet duis. Faucibus est feugiat ut auctor sapien, et. Eget tortor tortor, non morbi et odio. Lectus sed in rhoncus euismod eros, velit convallis ornare est. Eu amet cursus ut eros. Elementum sed etiam arcu etiam id. A et sit nulla varius turpis commodo viverra augue. Pulvinar magna nulla dignissim a. Eu tristique odio ultrices euismod nunc semper neque. Interdum egestas consequat habitasse lectus nam eget diam. Et cras at aliquet feugiat ut varius. Senectus quam ante enim orci vitae sit tempor iaculis feugiat. Morbi at mi etiam aliquet eget urna scelerisque. Risus sed est augue convallis posuere egestas ridiculus nunc, pellentesque.',
    para2:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor cursus pulvinar mauris posuere tellus nisi, velit massa integer. Netus urna morbi eget sit duis ante aliquet duis. Faucibus est feugiat ut auctor sapien, et. Eget tortor tortor, non morbi et odio. Lectus sed in rhoncus euismod eros, velit convallis ornare est. Eu amet cursus ut eros. Elementum sed etiam arcu etiam id. A et sit nulla varius turpis commodo viverra augue. Pulvinar magna nulla dignissim a. Eu tristique odio ultrices euismod nunc semper neque. Interdum egestas consequat habitasse lectus nam eget diam. Et cras at aliquet feugiat ut varius. Senectus quam ante enim orci vitae sit tempor iaculis feugiat. Morbi at mi etiam aliquet eget urna scelerisque. Risus sed est augue convallis posuere egestas ridiculus nunc, pellentesque.',
    para3:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor cursus pulvinar mauris posuere tellus nisi, velit massa integer. Netus urna morbi eget sit duis ante aliquet duis. Faucibus est feugiat ut auctor sapien, et. Eget tortor tortor, non morbi et odio. Lectus sed in rhoncus euismod eros, velit convallis ornare est. Eu amet cursus ut eros.',
    contentImage: NewsContentImage1
  }
]

export const blogDetails:detailsObject[] = [
  {
    id: 1,
    comments: [
      {
        userName: 'Alex Morgan',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Emili Fox',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Piers Morgan',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Tucker Carlson',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Sean Hannity',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Ingraham Angle',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'John Doe',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      }
    ],
    title: 'Blog Title',
    pageBanner: BannerImage1,
    contentBanner: BlogBanner1,
    category: 'Health',
    postedAt: "March 12, 2020",
    para1:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor cursus pulvinar mauris posuere tellus nisi, velit massa integer. Netus urna morbi eget sit duis ante aliquet duis. Faucibus est feugiat ut auctor sapien, et. Eget tortor tortor, non morbi et odio. Lectus sed in rhoncus euismod eros, velit convallis ornare est. Eu amet cursus ut eros. Elementum sed etiam arcu etiam id. A et sit nulla varius turpis commodo viverra augue. Pulvinar magna nulla dignissim a. Eu tristique odio ultrices euismod nunc semper neque. Interdum egestas consequat habitasse lectus nam eget diam. Et cras at aliquet feugiat ut varius. Senectus quam ante enim orci vitae sit tempor iaculis feugiat. Morbi at mi etiam aliquet eget urna scelerisque. Risus sed est augue convallis posuere egestas ridiculus nunc, pellentesque.',
    para2:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor cursus pulvinar mauris posuere tellus nisi, velit massa integer. Netus urna morbi eget sit duis ante aliquet duis. Faucibus est feugiat ut auctor sapien, et. Eget tortor tortor, non morbi et odio. Lectus sed in rhoncus euismod eros, velit convallis ornare est. Eu amet cursus ut eros. Elementum sed etiam arcu etiam id. A et sit nulla varius turpis commodo viverra augue. Pulvinar magna nulla dignissim a. Eu tristique odio ultrices euismod nunc semper neque. Interdum egestas consequat habitasse lectus nam eget diam. Et cras at aliquet feugiat ut varius. Senectus quam ante enim orci vitae sit tempor iaculis feugiat. Morbi at mi etiam aliquet eget urna scelerisque. Risus sed est augue convallis posuere egestas ridiculus nunc, pellentesque.',
    para3:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor cursus pulvinar mauris posuere tellus nisi, velit massa integer. Netus urna morbi eget sit duis ante aliquet duis. Faucibus est feugiat ut auctor sapien, et. Eget tortor tortor, non morbi et odio. Lectus sed in rhoncus euismod eros, velit convallis ornare est. Eu amet cursus ut eros.',
    contentImage: BlogContentImage1
  },
  {
    id: 2,
    comments: [
      {
        userName: 'Alex Morgan',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Emili Fox',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Piers Morgan',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Tucker Carlson',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Sean Hannity',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Ingraham Angle',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'John Doe',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      }
    ],
    title: 'Lorem ipsum dolor sit amet, consecteur adipiscing elit.',
    pageBanner: BannerImage1,
    contentBanner: BlogBanner1,
    category: 'Health',
    postedAt: "March 12, 2020",
    para1:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor cursus pulvinar mauris posuere tellus nisi, velit massa integer. Netus urna morbi eget sit duis ante aliquet duis. Faucibus est feugiat ut auctor sapien, et. Eget tortor tortor, non morbi et odio. Lectus sed in rhoncus euismod eros, velit convallis ornare est. Eu amet cursus ut eros. Elementum sed etiam arcu etiam id. A et sit nulla varius turpis commodo viverra augue. Pulvinar magna nulla dignissim a. Eu tristique odio ultrices euismod nunc semper neque. Interdum egestas consequat habitasse lectus nam eget diam. Et cras at aliquet feugiat ut varius. Senectus quam ante enim orci vitae sit tempor iaculis feugiat. Morbi at mi etiam aliquet eget urna scelerisque. Risus sed est augue convallis posuere egestas ridiculus nunc, pellentesque.',
    para2:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor cursus pulvinar mauris posuere tellus nisi, velit massa integer. Netus urna morbi eget sit duis ante aliquet duis. Faucibus est feugiat ut auctor sapien, et. Eget tortor tortor, non morbi et odio. Lectus sed in rhoncus euismod eros, velit convallis ornare est. Eu amet cursus ut eros. Elementum sed etiam arcu etiam id. A et sit nulla varius turpis commodo viverra augue. Pulvinar magna nulla dignissim a. Eu tristique odio ultrices euismod nunc semper neque. Interdum egestas consequat habitasse lectus nam eget diam. Et cras at aliquet feugiat ut varius. Senectus quam ante enim orci vitae sit tempor iaculis feugiat. Morbi at mi etiam aliquet eget urna scelerisque. Risus sed est augue convallis posuere egestas ridiculus nunc, pellentesque.',
    para3:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor cursus pulvinar mauris posuere tellus nisi, velit massa integer. Netus urna morbi eget sit duis ante aliquet duis. Faucibus est feugiat ut auctor sapien, et. Eget tortor tortor, non morbi et odio. Lectus sed in rhoncus euismod eros, velit convallis ornare est. Eu amet cursus ut eros.',
    contentImage: BlogContentImage1
  },
  {
    id: 3,
    comments: [
      {
        userName: 'Alex Morgan',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Emili Fox',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Piers Morgan',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Tucker Carlson',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Sean Hannity',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'Ingraham Angle',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      },
      {
        userName: 'John Doe',
        userProfileImage: CommentorImage1,
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus tempora cupiditate earum natus aut corporis.'
      }
    ],
    title: 'Lorem ipsum dolor sit amet, consecteur adipiscing elit.',
    pageBanner: BannerImage1,
    contentBanner: BlogBanner1,
    category: 'Health',
    postedAt: "March 12, 2020",
    para1:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor cursus pulvinar mauris posuere tellus nisi, velit massa integer. Netus urna morbi eget sit duis ante aliquet duis. Faucibus est feugiat ut auctor sapien, et. Eget tortor tortor, non morbi et odio. Lectus sed in rhoncus euismod eros, velit convallis ornare est. Eu amet cursus ut eros. Elementum sed etiam arcu etiam id. A et sit nulla varius turpis commodo viverra augue. Pulvinar magna nulla dignissim a. Eu tristique odio ultrices euismod nunc semper neque. Interdum egestas consequat habitasse lectus nam eget diam. Et cras at aliquet feugiat ut varius. Senectus quam ante enim orci vitae sit tempor iaculis feugiat. Morbi at mi etiam aliquet eget urna scelerisque. Risus sed est augue convallis posuere egestas ridiculus nunc, pellentesque.',
    para2:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor cursus pulvinar mauris posuere tellus nisi, velit massa integer. Netus urna morbi eget sit duis ante aliquet duis. Faucibus est feugiat ut auctor sapien, et. Eget tortor tortor, non morbi et odio. Lectus sed in rhoncus euismod eros, velit convallis ornare est. Eu amet cursus ut eros. Elementum sed etiam arcu etiam id. A et sit nulla varius turpis commodo viverra augue. Pulvinar magna nulla dignissim a. Eu tristique odio ultrices euismod nunc semper neque. Interdum egestas consequat habitasse lectus nam eget diam. Et cras at aliquet feugiat ut varius. Senectus quam ante enim orci vitae sit tempor iaculis feugiat. Morbi at mi etiam aliquet eget urna scelerisque. Risus sed est augue convallis posuere egestas ridiculus nunc, pellentesque.',
    para3:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor cursus pulvinar mauris posuere tellus nisi, velit massa integer. Netus urna morbi eget sit duis ante aliquet duis. Faucibus est feugiat ut auctor sapien, et. Eget tortor tortor, non morbi et odio. Lectus sed in rhoncus euismod eros, velit convallis ornare est. Eu amet cursus ut eros.',
    contentImage: BlogContentImage1
  }
]
