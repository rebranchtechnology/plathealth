import React from 'react'
import BtnComponent from './Button';

function Pagination() {
    return (
        <div className="flex flex-row flex-wrap gap-2 lg:gap-6">
            <BtnComponent text={'PREV'} width="w-full" />
            <BtnComponent text={'1'} width="w-full" />
            <BtnComponent text={'2'} width="w-full" />
            <BtnComponent text={'3'} width="w-full" />
            <BtnComponent text={'4'} width="w-full" />
            <BtnComponent text={'NEXT'} width="w-full" />
        </div>
    )
}

export default Pagination
