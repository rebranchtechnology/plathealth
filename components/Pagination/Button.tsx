import { FC, MouseEventHandler, ReactNode } from 'react'
import Link from 'next/link'

type ButtonProps = {
  text: string;
  width?: string;
  path?: string;
}

const BtnComponent: FC<ButtonProps> = ({ text, width, path }: ButtonProps) => {

  let btn: ReactNode;

  let textColor: string = '';
  if (text === 'NEXT' || text === 'PREV') {
      textColor = 'text-cardBlue';
  }

  if (path) {
    btn = <Link href={`${path}`}>
      <span className={`${textColor}hover:text-white hover:bg-cardBlue text-base text-center border border-cardBlue cursor-pointer p-4 ${width}`}><a>{text}</a></span>
    </Link>;
  } else {
    btn = <span className={`${textColor} hover:text-white hover:bg-cardBlue text-base text-center border border-cardBlue cursor-pointer p-4 ${width}`}><a>{text}</a></span>
  }

  return (
    <div className="flex justify-center p-1">
      {btn}
    </div>
  )
}

export default BtnComponent;