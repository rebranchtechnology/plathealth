import MainContainer from '@components/Container'
import Image from 'next/image'
import TextContent from '@components/common/TextContent'
import CardImage from '@components/common/CardImage'
import FlexWrapper from '@components/Container/FlexWrapper'
import CardText from '@components/common/CardText'
import { HeartIcon } from '@heroicons/react/outline'
import { HomeIcon } from '@heroicons/react/outline'
import { UserGroupIcon } from '@heroicons/react/outline'
import { ClockIcon } from '@heroicons/react/outline'
import { PlusIcon } from '@heroicons/react/outline'
import { ClipboardCheckIcon } from '@heroicons/react/outline'
import { ArrowCircleRightIcon } from '@heroicons/react/outline'

const services = [
  {
    name: 'Assistance with daily life tasks in a group or shared living arrangement',
    description:
      'We focus on assisting our participants in developing and nurturing their domestic and life skills to enhance their independence. We deliver these services in shared accommodation or in purpose-built homes, taking care to always ensure the privacy and personal space of participants. In this arrangement, we provide one-on-one support and assist with daily tasks.',
    icon: UserGroupIcon
  },
  {
    name: 'Participation in Community, Social and Civic Activities',
    description:
      'At Medela Health, we assist our participants by helping them participate in the community and facilitating their engagement in social activities. We believe that social connection and community engagement are crucial aspects of daily life, we provide a variety of one-on-one community access, community outreach activities and community group activities for people with disabilities, according to their goals and passions.',
    icon: HomeIcon
  },
  {
    name: 'Assistance with Daily Personal Activity',
    description:
      "Our assistance with daily personal care activities is tailored around the individual and their needs. These personal care services are adaptable, and custom built to the individual's specific needs and objectives. We not only assist our participants with their chores, but we also assist them in developing their independence.",
    icon: ClipboardCheckIcon
  },
  {
    name: 'Assistance with Travel/Transport',
    description:
      'People with disabilities benefit from our exceptional travel and transportation services. Supports that enable participants to build capacity to travel independently, such as personal transportation-related ids and equipment or instructions to use public transportation, are examples of travel and transportation assistance we provide at Medela Health.',
    icon: ArrowCircleRightIcon
  },
  {
    name: 'Respite',
    description:
      'Respite is short to medium term accommodation services that enable people with disabilities and their families to be independent and accomplish their goals in a supportive environment. Respite facilities also provide the opportunity for families and carers to take short to medium term breaks from their daily routines.',
    icon: HeartIcon
  },
  {
    name: 'Community Nursing',
    description:
      "The Medela Health team is caring, friendly, professional, and compliant with all NDIS Practice Standards. We are committed to ensuring you remain in control of the nursing services we deliver. Whether it's overnight care, respite or ongoing health management, our highly-credentialed Community Nurses are culturally sensitive and can liaise with your regular GP and other health providers, so you can be sure your needs are being met by a trusted team.",
    icon: PlusIcon
  }
]

export default function OurServices() {
  return (
    <MainContainer>
      <div className='lg:text-center'>
        <h2 className='text-2xl text-greenie font-semibold tracking-wide uppercase'>Services</h2>
        <p className='mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl'>
          Quality care, delivered by quality teams
        </p>
        <p className='mt-4 max-w-2xl text-xl text-gray-500 lg:mx-auto'>
          At Medela, care comes first. We believe in personalising our care services to their needs
          and involving them and their support network in meeting their goals, growing their passions, and enabling them
          to live as independently, and comfortablty as possible.
        </p>
      </div>

      <div className='mt-10'>
        <dl className='space-y-10 md:space-y-0 md:grid md:grid-cols-2 md:gap-x-8 md:gap-y-10'>
          {services.map(service => (
            <div key={service.name} className='relative'>
              <dt>
                <div className='absolute flex items-center justify-center h-12 w-12 rounded-md bg-greenie text-white'>
                  <service.icon className='h-6 w-6' aria-hidden='true' />
                </div>
                <p className='ml-16 text-lg leading-6 font-medium text-gray-900'>{service.name}</p>
              </dt>
              <dd className='mt-2 ml-16 text-base text-gray-500'>{service.description}</dd>
            </div>
          ))}
        </dl>
      </div>
      <div>
        <p className='flex justify-center mt-2 text-xl leading-8 font-semi-bold tracking-tight text-gray-900 sm:text-3xl'>
          Are you currently searching for a reliable service provider?
        </p>
        <p className='mt-4 max-w-2xl text-xl text-gray-500 lg:mx-auto'>
          Our disability support workers are high trained and experienced with dealing with a wide range of complex
          cases, they are understanding, caring, nurturing individuals that you will ever come across. They are also
          accredited and certified in disabilities and mental health training, first aid certification, manual handling
          training, working with children checks, and police clearances. You can be assured that we will go above and
          beyond to find the right mix of support to enable individuals and their families in achieving their goals no
          matter how big or small. Reach out today to get in touch with our team.
        </p>
      </div>
    </MainContainer>
  )
}
