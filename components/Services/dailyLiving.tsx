import React, { FC } from 'react'

import BodyContainer from '@components/Container/BodyContainer'
import MainContainer from '@components/Container'
import TextContent from '@components/common/TextContent'

import CardImage from '@components/common/CardImage'
import img1 from '@assets/images/coreSupport/independent/img1.png'
import img2 from '@assets/images/coreSupport/independent/img2.png'
import img3 from '@assets/images/coreSupport/independent/img3.png'
import img4 from '@assets/images/coreSupport/independent/img4.png'
import img5 from '@assets/images/coreSupport/independent/img5.png'
import img6 from '@assets/images/coreSupport/independent/img6.png'
import FlexWrapper from '@components/Container/FlexWrapper'
const services = [
  {
    img: img1,
    title: '24 / 7 & over-night support'
  },
  {
    img: img2,
    title: 'Clinical / nursing support'
  },
  {
    img: img3,
    title: 'Social & community activities'
  },
  {
    img: img4,
    title: 'Drop in support'
  },
  {
    img: img5,
    title: 'Medication management'
  },
  {
    img: img6,
    title: 'Individual goal planning'
  }
]

export default function ServicesDailyLiving() {
  return (
    <BodyContainer>
      <MainContainer>
        <div className='mt-8 '>
          <TextContent
            head={'Assistance with daily living in a group/shared arrangement'}
            para={
              'Our focus is on assisting our participants in developing and nurturing their domestic and life skills to enhance their independence. We deliver these services in shared accommodation or in purpose-built homes, taking care to always ensure the privacy and personal space of participants. In this arrangement, we provide one-on-one support and assist with daily tasks.'
            }
            center
          />
        </div>

        <FlexWrapper>
          {services.map((item, idx) => (
            <CardImage key={idx} img={item.img} title={item.title}></CardImage>
          ))}
        </FlexWrapper>
      </MainContainer>
    </BodyContainer>
  )
}
