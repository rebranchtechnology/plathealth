import BaseLayout from '@layouts/Base'
import { pageRoutes } from '@lib/routes'
import Image from 'next/image'

import img1 from '@assets/images/coreSupport/community/img1.png'
import img2 from '@assets/images/coreSupport/community/img2.png'
import img3 from '@assets/images/coreSupport/community/img3.png'
import img4 from '@assets/images/coreSupport/community/img4.png'
import img5 from '@assets/images/coreSupport/community/img5.png'
import img6 from '@assets/images/coreSupport/community/img6.png'

import MainContainer from '@components/Container/index'
import BodyContainer from '@components/Container/BodyContainer'
import TextContent from '@components/common/TextContent'
import FlexWrapper from '@components/Container/FlexWrapper'
import CardImage from '@components/common/CardImage'

export default function CommunityParticipation() {
  const services = [
    {
      img: img1,
      title: 'Travel & transport'
    },
    {
      img: img2,
      title: 'Exercise & sports'
    },
    {
      img: img3,
      title: 'Volunteering & working'
    },
    {
      img: img4,
      title: 'Visiting family & friends'
    },
    {
      img: img5,
      title: 'Civic & arts events'
    },
    {
      img: img6,
      title: 'Centre-based activities'
    }
  ]

  return (
    <BodyContainer>
      <MainContainer>
        <div className='mt-8 lg:mt-8 xl:mt-8'>
          <TextContent
            head={'Driving your passions in your community'}
            para={
              "At Medela Health, we want to improve every aspect of your life. That's why we offer services that help you to live independently, participate in your local community and benefit from the growth that comes with achieving your goals and growing your passions."
            }
            center
          />
        </div>

        <FlexWrapper>
          {services.map((item, idx) => (
            <CardImage key={idx} img={item.img} title={item.title}></CardImage>
          ))}
        </FlexWrapper>
      </MainContainer>
    </BodyContainer>
  )
}
