import React, { FC } from 'react'

import BodyContainer from '@components/Container/BodyContainer'
import MainContainer from '@components/Container'
import TextContent from '@components/common/TextContent'

import CardImage from '@components/common/CardImage'
import img1 from '@assets/images/coreSupport/household/img1.png'
import img2 from '@assets/images/coreSupport/household/img2.png'
import img3 from '@assets/images/coreSupport/household/img3.png'
import img4 from '@assets/images/coreSupport/household/img4.png'
import img5 from '@assets/images/coreSupport/household/img5.png'
import img6 from '@assets/images/coreSupport/household/img6.png'
import FlexWrapper from '@components/Container/FlexWrapper'

export default function DailyLiving() {
  const services = [
    {
      img: img1,
      title: 'Cleaning and gardening'
    },
    {
      img: img2,
      title: 'Personal care'
    },
    {
      img: img3,
      title: 'Cooking & meal prep'
    },
    {
      img: img4,
      title: 'Shopping'
    },
    {
      img: img5,
      title: 'In-home therapies'
    },
    {
      img: img6,
      title: 'Physical activity & exercise'
    }
  ]

  return (
    <BodyContainer>
      <MainContainer>
        <div className='mt-8'>
          <TextContent
            head={'Helping you stay on top of every day'}
            para={
              'When it comes to daily personal living activities, we understand that it not easy for people living with disability to perform daily personal living activities and we are here to help. Our team is highly experienced in assisting our participants with daily living tasks including regular tasks and chores that are performed daily. This includes daily tasks such as getting out of bed, showering, eating, and moving around.'
            }
          />
        </div>
        <FlexWrapper>
          {services.map((item, idx) => (
            <CardImage key={idx} img={item.img} title={item.title}></CardImage>
          ))}
        </FlexWrapper>
      </MainContainer>
    </BodyContainer>
  )
}
