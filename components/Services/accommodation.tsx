import img1 from '@assets/images/accommodation/img1.png'
import img2 from '@assets/images/accommodation/img2.png'
import img3 from '@assets/images/accommodation/img3.png'

import MainContainer from '@components/Container/index'
import BodyContainer from '@components/Container/BodyContainer'
import TextContent from '@components/common/TextContent'
import Image from 'next/image'
import { pageRoutes } from '@lib/routes'
import React, { FC } from 'react'

export default function Accommodation() {
  return (
    <BodyContainer>
      <MainContainer>
        <div className='realtive px-4'>
          <div className='pt-8'>
            <TextContent
              head={'Short Term Accomodation or Respite: Change when you need it'}
              para={
                'We provide short-term accomodation to expose our clients to different environments where they can still access the same level of support.'
              }
            />
            <p className='text-sm sm:text-base md:max-w-3xl mb-4'>This covers:</p>
            <ul className='list-disc max-w-md mx-auto text-sm sm:text-base pb-8 md:max-w-3xl'>
              <li>One-on-one short term care</li>
              <li>Group-based short term options</li>
              <li>Overnight or weekend support facilities</li>
            </ul>
            <Image src={img1} alt='medium term accommodation image mum in wheelchair with daughter' />
          </div>
          <div className='pt-8'>
            {' '}
            <TextContent
              head={'Medium Term Accomodation: For transitions to permanent Housing'}
              para={
                'We offer Medium Term Accommodation to participants who require temporary housing while they transition to their permanent home for any of the reasons below:'
              }
            />
            <ul className='list-disc max-w-md mx-auto text-sm sm:text-base pb-8 md:max-w-3xl'>
              <li>Participants waiting for Specialist Disability Accomodation</li>
              <li>People who have otherwise been found eligible for Specialist Disability Accomodation</li>
              <li>
                Those who wish to leave aged care while waiting for home modifications or identified Specialist
                Disability Accomodation
              </li>
            </ul>
            <Image src={img2} alt='medium term accommodation image mum in wheelchair with daughter' />
          </div>
          <div className='pt-8'>
            {' '}
            <TextContent
              head={'Specialist Disability Accomodation'}
              para={
                'To help address the lack of accessible housing options for people with disability, the NDIS has created a new funding stream called Specialist Disability Accommodation (SDA). SDA services enable people with disability to live as independently as possible through funding that encourages the development of purpose-built housing options. We help participants understand and find suitable accommodation where necessary.'
              }
            />
            <div className='pt-4'>
              {' '}
              <Image src={img3} alt='specialist disability accommodation image mum in wheelchair with daughter' />
            </div>
          </div>
          <div className='pt-8'>
            <TextContent
              head={'Available Accomodation'}
              para={
                'We have a number of properties available through the Perth Metropolitan area and often co-ordinate with participants support networks to help them find a place where they can be comfortable and close to their friends and community. Please contact us for more details.'
              }
            />
            <button
              type='button'
              className='uppercase bg-purpie text-white py-2 md:w-1/4 w-full mt-4 rounded-md hover:bg-greenie'
            >
              <a href={pageRoutes.contact}>Contact Us</a>
            </button>
          </div>
        </div>
      </MainContainer>
    </BodyContainer>
  )
}
