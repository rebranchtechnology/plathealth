import BaseLayout from '@layouts/Base'
import { pageRoutes } from '@lib/routes'
export default function ServicesIndependentLivingPage() {
  return (
    <BaseLayout title='Services' selectedNavKey='services' description='Services' canonical={pageRoutes.services}>
      <div className='mb-auto md:mx-auto py-6 sm:px-6 lg:px-8 grid'>
        <main className='mx-auto max-w-7xl px-4'>
          <div className='text-center'>
            <h1 className='text-4xl tracking-tight font-extrabold text-gray-900 sm:text-5xl md:text-6xl'>
              <span className='block xl:inline'>Independent</span>{' '}
              <span className='block text-red-600 xl:inline'>Living Options</span>
            </h1>
            <p className='mt-3 max-w-md mx-auto text-base text-gray-500 sm:text-lg md:mt-5 md:text-xl md:max-w-3xl'>
              We support our clients with help and/or supervision of daily tasks to help them live as independently as
              possible including grocery shopping, meal planning, administering medication, assistance with personal
              care, attendance of medical appointments and supporting their hobbies and passions.
            </p>
          </div>
        </main>
      </div>
    </BaseLayout>
  )
}
