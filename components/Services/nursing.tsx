import BaseLayout from '@layouts/Base'
import { pageRoutes } from '@lib/routes'
import Image from 'next/image'

import img1 from '@assets/images/communityNursing/img1.png'
import img2 from '@assets/images/communityNursing/img2.png'

import MainContainer from '@components/Container/index'
import BodyContainer from '@components/Container/BodyContainer'
import TextContent from '@components/common/TextContent'
import FlexWrapper from '@components/Container/FlexWrapper'
import { PlusCircleIcon } from '@heroicons/react/outline'

export default function CommunityNursing() {
  const nursingServices = [
    {
      title: 'Medication management'
    },
    {
      title: 'Diabetes management'
    },
    {
      title: 'Cannulation'
    },
    {
      title: 'General health checks'
    },
    {
      title: 'Catheter care'
    },
    {
      title: 'Overnight nursing'
    },
    {
      title: 'Palliative care'
    },
    {
      title: 'Complex wound management'
    },
    {
      title: 'Simple & complex dressings'
    },
    {
      title: 'PEG feeding'
    },
    {
      title: 'Clinical needs assessments'
    },
    {
      title: 'Respiratory support'
    }
  ]

  const paraList = [
    'The Medela Health team is caring, friendly and professional, complying with all NDIS Practice Standards.',
    "We are committed to ensuring you remain in control of the nursing services we deliver. Whether it's overnight care, respite or ongoing health management, our highly-credentialed Community Nurses are culturally sensitive and can liaise with your regular GP and other health providers, so you can be sure your needs are being met by a trusted team."
  ]

  return (
    <BodyContainer>
      <MainContainer>
        <div className='bg-bgGray flex justify-between items-stretch'>
          <div className='relative lg:w-1/2 h-auto hidden lg:block'>
            <Image src={img1} alt='Nurse and patient in home Image' layout='fill' objectFit='cover' />
          </div>
          <div className='w-11/12 m-auto lg:w-1/2 h-825 py-16 lg:py-20 xl:py-24 lg:px-16'>
            <TextContent
              head={
                'Wherever you are WA-wide our team of registered and enrolled nurses can provide exceptional clinical care.'
              }
              para={paraList}
            />
          </div>
        </div>
        <div className='mt-8 '>
          <h1 className='text-center text-greenie font-normal lg:text-center'> Our Services Cover... </h1>
          <div className='grid grid-cols-3'>
            {nursingServices.map((item, idx) => (
              <div key={idx} className='col-span-1 '>
                <ul className='list-inside ...'>
                  <li className='mt-4 flex gap-4 items-center'>
                    <PlusCircleIcon className='h-12 w-12 text-purpie' />
                    <p className='font-bold flex-1'>{item.title}</p>
                  </li>
                </ul>
              </div>
            ))}
          </div>
        </div>
        <FlexWrapper>
          <Image src={img2} alt='nursing image two' />
        </FlexWrapper>
      </MainContainer>
    </BodyContainer>
  )
}
