import React, { FC } from 'react'

const categories = ['HEALTH', 'LOVE', 'MEDICINE', 'SUPPORT', 'AWARENESS']

const SearchCategories: FC = () => {
  return (
    <div className='bg-gray-200 flex flex-col py-5'>
      <h1 className='text-xl mt-0 pt-0 w-11/12 mx-auto'>Categories</h1>
      <div className=''>
        {categories?.map((item, idx) => {
          return (
            <div key={idx} className='mt-5 bg-white text-black w-11/12 mx-auto py-3 px-5'>
              <span className='font-bold'>{item}</span>
            </div>
          )
        })}
      </div>
    </div>
  )
}

export default SearchCategories
