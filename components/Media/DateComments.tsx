import React, { FC } from 'react'

import Image from 'next/image'

import CalendarIcon from '@assets/images/news/calendar.png'
import CommentIcon from '@assets/images/news/comment.png'

interface DateCommentsProps {
  comments: string
  date: string
}

const DateComments: FC<DateCommentsProps> = ({ date, comments }) => {
  return (
    <div className='flex flex-1 justify-between'>
      <div className='flex flex-1 items-center'>
        <Image src={CalendarIcon} alt='Calendar Icon' />
        <p className='pl-4 text-sm text-black xl:text-xs'> {date} </p>
      </div>
      <div className='flex flex-1 items-center'>
        <Image src={CommentIcon} alt='Comment Icon' />
        <p className='pl-4 text-sm text-black xl:text-xs'> {comments} </p>
      </div>
    </div>
  )
}

export default DateComments
