import React, { FC } from 'react'

interface CategoryProps {
  backgroundColor: string
  name: string
  textColor: string
}

const Category: FC<CategoryProps> = ({ backgroundColor, name, textColor }) => {
  return (
    <div className={`inline-block px-6 text-sm uppercase bg-${backgroundColor} text-${textColor} xl:px-3`}>
      <p className='text-xs'> {name} </p>
    </div>
  )
}

export default Category
