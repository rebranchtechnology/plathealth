import React, { FC } from 'react'

const MainImage: FC = () => {
  return (
    <div className='relative'>
      <div className='bg-news-blogs bg-center bg-cover w-full h-60 bg-no-repeat flex justify-center items-center filter brightness-50 sm:h-60 md:h-65 lg:bg-contact-us-web'></div>
      <h1 className='absolute text-3xl text-center tracking-normal font-extrabold text-white top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 sm:text-5xl lg:text-6xl'>
        <span className='block xl:inline'>NEWS &amp; BLOGS </span>
      </h1>
    </div>
  )
}

export default MainImage
