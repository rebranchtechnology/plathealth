import React, { FC } from 'react'
import Image from 'next/image'
export interface CommentsSectionProps {
  userName: string
  userProfileImage: StaticImageData
  text: string
}

const CommentsSection: FC<{ comments: CommentsSectionProps[] }> = ({ comments }) => {
  return (
    <div className='border-t-2 border-b-2 border-gray-500 bg-gray-200 py-6'>
      <div className='px-5'>
        <h1 className='pt-0 text-xl'> {comments.length} Comments </h1>
      </div>
      <div className='overflow-y-auto max-h-56 mt-5 px-5 space-y-4'>
        {comments?.map(({ text, userName, userProfileImage }, idx) => {
          return (
            <div key={idx} className='flex gap-5'>
              <div>
                <Image src={userProfileImage} className='border border-gray-900' height={100} width={100} />
              </div>
              <div>
                <p className='pt-0 font-francois font-medium text-xl'>{userName}</p>
                <p className='leading-6'>{text}</p>
              </div>
            </div>
          )
        })}
      </div>
    </div>
  )
}

export default CommentsSection
