import React, { FC } from 'react'

const SearchTopics: FC = () => {
  return (
    <div className='bg-gray-200 flex flex-col py-5'>
      <h1 className='text-xl mt-0 pt-0 w-11/12 mx-auto'>Search Topics</h1>
      <div className="w-11/12 mx-auto">
        <input
          className='shadow w-full appearance-none rounded-sm not-italic outline-none py-3 h-14 border border-darkgray-700 mt-4 text-gray-700 leading-tight'
          id='name'
          type='text'
          placeholder='Search here'
        />
      </div>
    </div>
  )
}

export default SearchTopics
