import React, { FC } from 'react'

const ReadMore: FC = () => {
  return (
    <div className='px-5 inline-block bg-yellie text-white py-1 cursor-pointer xl:py-0 xl:px-3'>
      <p>READ MORE</p>
    </div>
  )
}

export default ReadMore
