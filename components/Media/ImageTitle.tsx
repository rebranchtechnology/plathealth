import React, { FC } from 'react'

interface ImageTitleProps {
  title: string
}

const ImageTitle: FC<ImageTitleProps> = ({ title }) => {
  return <h1 className='text-xl text-mBlack xl:pt-0'> {title} </h1>
}

export default ImageTitle
