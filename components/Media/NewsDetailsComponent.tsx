import { FC } from 'react'
import SearchCategories from './SearchCategories'
import SearchTopics from './SearchTopics'
import Image from 'next/image'
import { blogDetails, detailsObject, newsDetails } from 'mock/newsandblogsdata'
import { ChatAltIcon, CalendarIcon } from '@heroicons/react/solid'
import CommentsSection from './CommentsSection'
import ContactSection from './ContactSection'

interface ArticleProps extends detailsObject {
  type?: string
}

const ArticleComponent: FC<ArticleProps> = ({ id, title, category, comments, postedAt, para1, para2, para3, type }) => {
  const data = type === 'blog' ? blogDetails.find(news => news.id == id) : newsDetails.find(news => news.id == id)
  const contentBanner = data?.contentBanner
  const contentImage = data?.contentImage
  let commentData = data?.comments
  return (
    <div className='bg-white py-10 w-full'>
      <div className='w-4/5 mx-auto grid gap-8 lg:grid-cols-7'>
        <div className='col-span-full lg:col-span-5 space-y-6'>
          <div className='space-y-2'>
            {contentBanner && <Image src={contentBanner} alt='news article image' />}
            <div className='flex flex-wrap gap-4 items-center'>
              <p className='uppercase px-4 text-white bg-green-400'>{category}</p>
              <p className='flex items-center text-sm space-x-2 font-medium'>
                <CalendarIcon className='w-6 h-6 text-pink-700' />
                <span>{postedAt}</span>
              </p>
              <p className='flex items-center text-sm space-x-2 font-medium'>
                <ChatAltIcon className='w-6 h-6 text-pink-700' />
                <span>{comments.length} Comments</span>
              </p>
            </div>
          </div>
          <div className='space-y-2'>
            <p className='text-3xl font-francois font-medium text-darkgray-700'>{title}</p>
            <p className='text-darkgray-700 leading-6'>{para1}</p>
            <p className='text-darkgray-700 leading-6'>{para2}</p>
          </div>
          <div className='lg:w-4/5 mx-auto'>
            {contentImage && <Image src={contentImage} alt='news content image' />}
          </div>
          <div>
            <p className='text-darkgray-700 leading-6'>{para3}</p>
          </div>
          <div>{commentData && <CommentsSection comments={commentData} />}</div>
          <div>
            <ContactSection />
          </div>
        </div>
        <div className='lg:col-span-2 space-y-10'>
          <SearchTopics />
          <SearchCategories />
        </div>
      </div>
    </div>
  )
}

export default ArticleComponent
