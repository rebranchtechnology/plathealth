import { FC } from 'react'
import { useForm } from 'react-hook-form'

import InputWithoutLabel from '../Form/Fields/InputWithoutLabel'
import TextAreaWithoutLabel from '../Form/Fields/TextAreaWithouLabel'

const ContactSection: FC = () => {
  const {
    register,
    handleSubmit,
    reset
  } = useForm()

  const onSubmit = data => {
    console.log('data: ', data);
    reset();
  }

  return (
    <form className='flex flex-col gap-4' onSubmit={handleSubmit(onSubmit)}>
      <h1 className='text-xl pt-2'> Leave a Comment </h1>
      <InputWithoutLabel
        registerFunction={register('name', { required: true })}
        name='name'
        type='text'
        placeholder='Your Full Name'
      />
      <InputWithoutLabel
        registerFunction={register('email', { required: true })}
        name='email'
        type='email'
        placeholder='Your Email'
      />
      <TextAreaWithoutLabel
        registerFunction={register('comment', { required: true })}
        name='comment'
        placeholder='Your Comments'
      />
      <button className='w-full py-1 uppercase bg-yellow-500 text-white' type='submit'>
        Submit
      </button>
    </form>
  )
}

export default ContactSection
