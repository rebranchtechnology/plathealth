import React, { useRef, useState } from 'react';
import Image from 'next/image'
import PlusRound from '@assets/images/PlusRound.png';
import MinusRound from '@assets/images/MinusRound.png';

interface AccordionProps {
  title: React.ReactNode
  content: React.ReactNode
}

 const Accordion: React.FC<AccordionProps> = ({ title, content }) => {
  const [active, setActive] = useState(false)
  const [height, setHeight] = useState('0px')
  const [rotate, setRotate] = useState('transform duration-700 ease')

  const contentSpace = useRef(null)

  function toggleAccordion() {
    setActive(active === false ? true : false)
    // @ts-ignore
    setHeight(active ? '0px' : `${contentSpace.current.scrollHeight}px`)
    setRotate(active ? 'transform duration-700 ease' : 'transform duration-700 ease rotate-180')
  }

  return (
    <div className="flex flex-col my-2">
      <button
        className="py-6 px-4 box-border appearance-none cursor-pointer focus:outline-none flex items-center justify-between   border-2 border-gray-600"
        onClick={toggleAccordion}
      >
        <div className="text-footnote light w-full ">{title}</div>
       {active &&
        <Image src={MinusRound} alt='collapse' />
  }
  {!active && 
  <Image src={PlusRound} alt='expand' />
  }
      </button>
      <div
        ref={contentSpace}
        style={{ maxHeight: `${height}` }}
        className="overflow-auto transition-max-height duration-700 ease-in-out"
      >
        <div className="pb-10 mt-4">{content}</div>
      </div>
    </div>
  )
}


export default Accordion