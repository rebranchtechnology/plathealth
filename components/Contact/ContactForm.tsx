import { Fragment, useEffect, useState } from 'react'
import { Transition } from '@headlessui/react'
import { CheckCircleIcon } from '@heroicons/react/outline'
import { XIcon } from '@heroicons/react/solid'
import { useForm, ValidationError } from '@formspree/react'
import React from 'react'

const ContactForm = () => {
  const [state, handleSubmit] = useForm('mayvbelg')
  const [submitted, onSubmitted] = useState(false)

  return (
    <>
      <div
        aria-live='assertive'
        className='absolute inset-0 z-40 flex items-end px-4 py-6 pointer-events-none sm:p-6 sm:items-start'
      >
        <div className='w-full flex flex-col items-center space-y-4 sm:items-end'>
          <Transition
            show={state.succeeded && submitted}
            as={Fragment}
            enter='transform ease-out duration-300 transition'
            enterFrom='translate-y-2 opacity-0 sm:translate-y-0 sm:translate-x-2'
            enterTo='translate-y-0 opacity-100 sm:translate-x-0'
            leave='transition ease-in duration-100'
            leaveFrom='opacity-100'
            leaveTo='opacity-0'
          >
            <div className='max-w-sm w-full bg-white shadow-lg rounded-lg pointer-events-auto ring-1 ring-black ring-opacity-5 overflow-hidden'>
              <div className='p-4'>
                <div className='flex items-start'>
                  <div className='flex-shrink-0'>
                    <CheckCircleIcon className='h-6 w-6 text-green-400' aria-hidden='true' />
                  </div>
                  <div className='ml-3 w-0 flex-1 pt-0.5'>
                    <p className='text-sm font-medium text-gray-900'>Email sent!</p>
                  </div>
                  <div className='ml-4 flex-shrink-0 flex'>
                    <button
                      className='bg-white rounded-md inline-flex text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-greenie'
                      onClick={() => {
                        onSubmitted(false)
                      }}
                    >
                      <span className='sr-only'>Close</span>
                      <XIcon className='h-5 w-5' aria-hidden='true' />
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </Transition>
        </div>
      </div>

      <div className='relative bg-white h-auto'>
        <div className='w-auto h-auto'>
          <div className='w-full mx-auto bg-white rounded-sm shadow-xl md:grid grid-cols-9 p-8 pb-12 gap-8 space-y-12 md:space-y-0'>
            <div className='col-span-5 lg:col-span-6'>
              <p className='font-medium xs:text-3xl text-xl text-greenie'>Send Us a Message</p>
              <form className='mt-8 space-y-12' onSubmit={handleSubmit}>
                <div>
                  <input
                    id='email'
                    type='email'
                    name='email'
                    className='w-full p-3 border-2 border-greenie rounded-sm'
                    placeholder='Your Email Address'
                  />
                  <ValidationError prefix='Email' errors={state.errors} field='email' />
                </div>
                <div>
                  <textarea
                    id='message'
                    name='message'
                    rows={5}
                    className='w-full p-3 border-2 border-greenie rounded-sm'
                    placeholder='Message here...'
                  />
                  <ValidationError prefix='Message' errors={state.errors} field='Message' />
                </div>
                <button
                  type='submit'
                  disabled={state.submitting}
                  className='uppercase bg-purpie text-white py-2 w-full rounded-sm hover:bg-greenie'
                  onClick={() => {
                    state?.submitting ? onSubmitted(false) : onSubmitted(true)
                  }}
                >
                  Submit
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default ContactForm
