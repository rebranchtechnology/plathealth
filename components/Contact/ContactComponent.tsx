import ContactForm from "./ContactForm"

const Description = ({lable, info}) => (
  <div className="text-center">
    <p className="text-2xl font-bold uppercase font-francois">{lable}</p>
    <p className="">{info}</p>
  </div>
)

export default function ContactComponent() {
  return (
    <div className='grid md:grid-cols-8 lg:grid-cols-7'>
      <div className='md:col-span-3 lg:col-span-2 bg-darkgray-700 text-white flex flex-col justify-between items-center py-10 px-10 space-y-6'>
        <Description lable='Address' info='56 Ellen Stirling Boulevard, Innaloo, WA 6018' />
        <Description lable='Phone' info='1300 415 409' />
        <Description lable='Office Hours' info='Monday - Friday: 9am-5pm' />
      </div>
      <div className="relative md:col-span-5 py-4 px-12 sm:px-20">
        <ContactForm />
      </div>
    </div>
  )
}