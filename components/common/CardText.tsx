import { FC } from 'react'
import Image from 'next/image'

type Props = {
  img: StaticImageData
  title: string
  para: string
  color?: 'greenie' | 'black'
}

const CardText: FC<Props> = ({ img, title, para, color, }: Props) => {
  return (
    <div className='w-full md:w-48% xl:w-32% flex flex-col gap-4 justify-between content-center items-center'>
      <Image src={img} alt={'Image'} />
      <h2 className={`text-${color ? 'greenie' : 'black'}`}>{title}</h2>
      <p className='text-center h-32'>{para}</p>
    </div>
  )
}
export default CardText
