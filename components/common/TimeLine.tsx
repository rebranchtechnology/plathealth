import { FC } from 'react'

interface TimeLineProps {
  labels: string[]
  currentIndex: number
}

const TimeLine: FC<TimeLineProps> = ({ labels, currentIndex }) => {
  return (
    <div className='flex md:flex-col justify-center pt-4 md:pl-4 text-white overflow-x-auto sm:overflow-x-visible'>
      {labels.map((label, i) => (
        <div
          key={i}
          className={`w-20 sm:w-24 md:w-auto flex-grow-0 flex-shrink-0 border-0 border-gray-500 h-24 relative ${
            i < currentIndex && 'text-yellow-500'
          }`}
        >
          {i !== labels.length - 1 && (
            <div
              className={`absolute h-1 md:h-full w-full md:w-1 ${
                i < currentIndex ? 'bg-yellow-500' : 'bg-gray-200'
              } inset-0 md:top-6 left-1/2 md:left-0`}
            ></div>
          )}
          <div
            className={`absolute h-5 w-5 -top-2 md:top-2 left-1/2 md:-left-2 transform -translate-x-1/2 md:translate-x-0 ${
              i <= currentIndex ? 'bg-yellow-500' : 'bg-gray-200'
            } rounded-full`}
          ></div>
          <div className='relative pt-8 md:pt-0 md:pl-6 text-center md:text-left break-all md:break-normal'>
            <p className='font-medium text-xs sm:text-sm md:text-lg font-francois'>{label}</p>
          </div>
        </div>
      ))}
    </div>
  )
}

export default TimeLine
