import { FC } from "react"

type Props = {
    head: string;
    para: string | Array<string>;
    center?: boolean;
    top?: boolean;
}

const TextContent: FC<Props> = ({ head, para, center, top }: Props) => {

    const align = center ? 'center' : 'left';
    const topBool = top ? null : 'p-0';

    return (
        <div className='flex flex-col justify-between gap-4'>
            <h1 className={`${topBool} text-center text-greenie font-normal lg:text-${align}`}> {head} </h1>
            {
                Array.isArray(para) && para.length > 0
                    ? para.map((item, idx) => (<p key={idx} className={`m-0 text-center lg:text-${align}`}> {item}</p>))
                    : <p className={`m-0 text-center lg:text-${align}`}> {para} </p>
            }
        </div>
    )
}

export default TextContent