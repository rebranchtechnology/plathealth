import { FC } from 'react'

interface Props {
  text?: string
  href?: string
  onChange?: (value: string, name?: string) => any
}

const MainButton: FC<Props> = ({ text, href, onChange }) => {
  return (
    <button
      type='button'
      className='inline-flex items-center px-4 py-2 border border-transparent text-base font-medium rounded-md shadow-sm text-white bg-greenie hover:bg-white hover:text-black focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
    >
      <a href={href}>{text}</a>
    </button>
  )
}
export default MainButton
