import { FC } from "react"
import Image from 'next/image'

type Props = {
    img: StaticImageData;
    title: string;
    link?: string;
    linkText?: string;
}

const CardImage: FC<Props> = ({ img, title, link, linkText }: Props) => {
    return (
        <div className='relative w-full h-full'>
            <Image src={img} alt={'Image'} layout='responsive'/>
            <div className='flex flex-col justify-center content-center items-center absolute top-2/3 w-full h-1/3'>
            </div>
            <h3 className='text-center text-greenie'>{title}</h3>
                {link && <a className='text-center underline text-greenie' href={link}>{linkText}</a>}
        </div>
    )
}

export default CardImage
