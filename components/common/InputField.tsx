import { FC } from 'react'

interface Props {
  placeholder?: string
  type?: string
  onChange?: (value: string, name?: string) => any
}

const InputField: FC<Props> = ({ placeholder, type, onChange }) => {
  const handleChange = val => {
    if (onChange) onChange(val?.target?.value)
  }

  return (
    <input
      className='w-full p-2 border border-gray-500'
      type={type ?? 'text'}
      placeholder={placeholder ?? 'Enter value'}
      onChange={handleChange}
    />
  )
}

export default InputField
