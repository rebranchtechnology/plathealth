import { FC, useEffect, useState } from 'react'

import { ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/solid'

interface Props {
  allData?: any
  currentPage: (page: number | string) => void
  paginatedData?: (data: any) => void
  size?: string | number
  total: number | string
}

const Pagination: FC<Props> = ({ total, currentPage, allData, paginatedData, size }) => {
  const [pages, setPages] = useState<(number | string)[]>([])
  const [activePage, setActivePage] = useState<number | string>(1)

  const onClickHandler = (page: number | string) => {
    setActivePage(page)
    currentPage(page)
  }

  useEffect(() => {
    if (total) {
      let arr: number[] = []
      for (let i = 0; i < total; i++) {
        arr.push(i + 1)
      }
      setPages(arr)
    }
  }, [total])

  return (
    <div className='bg-white px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6'>
      <div className='flex-1 flex justify-between sm:hidden'>
        <a
          href='#'
          className='relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50'
        >
          Previous
        </a>
        <a
          href='#'
          className='ml-3 relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50'
        >
          Next
        </a>
      </div>
      <div className='hidden sm:flex-1 sm:flex sm:items-center sm:justify-between'>
        <div>
          <nav className='relative z-0 inline-flex rounded-md shadow-sm -space-x-px' aria-label='Pagination'>
            <a
              href='#'
              className='relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50'
            >
              <span className='sr-only'>Previous</span>
              <ChevronLeftIcon className='h-5 w-5' aria-hidden='true' />
            </a>

            {pages &&
              pages.map((page, idx) => {
                return (
                  <a
                    key={idx}
                    onClick={() => onClickHandler(page)}
                    // href='#'
                    aria-current='page'
                    className={
                      page === activePage
                        ? 'z-10 bg-bluie text-white border-indigo-500 relative inline-flex items-center px-4 py-2 border text-sm font-medium'
                        : 'bg-white border-gray-300 text-gray-500 hover:bg-gray-50 hidden md:inline-flex relative items-center px-4 py-2 border text-sm font-medium'
                    }
                  >
                    {page}
                  </a>
                )
              })}

            <a
              href='#'
              className='relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50'
            >
              <span className='sr-only'>Next</span>
              <ChevronRightIcon className='h-5 w-5' aria-hidden='true' />
            </a>
          </nav>
        </div>
      </div>
    </div>
  )
}

export default Pagination
