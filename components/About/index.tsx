import BaseLayout from '@layouts/Base'
import { pageRoutes } from '@lib/routes'
import Image from 'next/image'
import aboutImg from '@assets/images/about/About.png'
import PlanImg from '@assets/images/about/Self.png'
import SelfImg from '@assets/images/about/Plan.png'
import NdiaImg from '@assets/images/about/Ndia.png'
import FlexWrapper from '@components/Container/FlexWrapper'
import CardText from '@components/common/CardText'

const about = [
  {
    img: SelfImg,
    alt: 'self managed image',
    head: 'Self Managed',
    desc: 'We work with you directly to get the most out of your funding.'
  },
  {
    img: PlanImg,
    alt: 'plan manage image',
    head: 'Plan Managed',
    desc: 'We work seamlessly with your preferred Plan Manager.'
  },
  {
    img: NdiaImg,
    alt: 'NDIA managed image',
    head: 'NDIA Managed',
    desc: 'As a registered provider, we can claim payment electronically from your funding.'
  }
]

export default function About() {
  return (
    <div className='relative max-w-7xl w-full h-full text-center'>
      <h1 className='text-4xl tracking-tight font-extrabold text-greenie sm:text-4xl '>About Us</h1>
      <p className='mt-4 max-w-md mx-auto text-lg text-black sm:text-xl md:max-w-3xl'>
        Medela Health, was founded by a group of like-minded and dedicated group to provide quality care services to
        West Australians. We work collaboratory with individuals and their families to shape a society where people with disabilities thrive. We aid individuals to accomplish their goals whilst also catering to their families and the support they need. 
      </p>
      <div className='w-full h-3/4 md:h-full lg:h-full object-cover grid justify-items-center mt-8'>
        <Image src={aboutImg} alt='About Medela Health Img' />
      </div>
    </div>
  )
}
