import { ReactNode, FC } from "react"

type Props = {
    children: ReactNode;
}
const FlexWrapper: FC<Props> = ({ children }: Props) => {
    return (
        <div className='flex flex-col md:flex-row flex-wrap gap-4 justify-between sm:mt-8 mt-4 bg-white px-8'>
            {children}
        </div>
    )
}

export default FlexWrapper
