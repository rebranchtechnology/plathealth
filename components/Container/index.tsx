import { FC, ReactNode } from "react"

type Props = {
children: ReactNode;
}

const index:FC<Props> = ({children} : Props) => {
    return (
        <div className='w-11/12 md:w-10/12 lg:w-9/12 m-auto bg-white'>
            {children}
        </div>
    )
}

export default index;
