import { FC, ReactNode } from "react"

type Props = {
    children: ReactNode;
}

const index: FC<Props> = ({ children }: Props) => {
    return (
        <div className='flex flex-col justify-around gap-16 lg:gap-18 mb-16 lg:mb-20 xl:mb-24 bg-white'>
            {children}
        </div>
    )
}

export default index;