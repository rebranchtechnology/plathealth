import { ReactNode } from 'react';

type Props = {
  children: ReactNode;
};

const index = ({ children }: Props) => {
  return (
    <div className='flex flex-col gap-10 md:gap-16 lg:gap-20 justify-center items-center w-11/12 md:w-10/12 lg:w-9/12 m-auto '>
      {children}
    </div>
  );
};

export default index;
