import { type } from 'os'
import { FC } from 'react'

type Props = {
    className?: string;
}

export const Calender: FC<{ className?: string }> = ({ className }: Props) => (
    <svg className={className ?? ''} width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M3.5 27.5C3.5 28.0531 3.94687 28.5 4.5 28.5H27.5C28.0531 28.5 28.5 28.0531 28.5 27.5V14.375H3.5V27.5ZM27.5 5.75H22.25V3.75C22.25 3.6125 22.1375 3.5 22 3.5H20.25C20.1125 3.5 20 3.6125 20 3.75V5.75H12V3.75C12 3.6125 11.8875 3.5 11.75 3.5H10C9.8625 3.5 9.75 3.6125 9.75 3.75V5.75H4.5C3.94687 5.75 3.5 6.19688 3.5 6.75V12.25H28.5V6.75C28.5 6.19688 28.0531 5.75 27.5 5.75Z" fill="#C24A5C" />
    </svg>
)

export const Comment: FC<{ className?: string }> = ({ className }: Props) => (
    <svg className={className ?? ''} width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M8.00078 3.19995H22.4008C24.1608 3.19995 25.6008 4.63995 25.6008 6.39995V17.6C25.6008 19.36 24.1608 20.7999 22.4008 20.7999H19.2008L11.2008 28.7999V20.7999H8.00078C6.24078 20.7999 4.80078 19.36 4.80078 17.6V6.39995C4.80078 4.63995 6.24078 3.19995 8.00078 3.19995Z" fill="#C24A5C" />
    </svg>

)