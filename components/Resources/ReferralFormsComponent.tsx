import TextContent from '@components/common/TextContent'
import TimeLine from '@components/common/TimeLine'
import MainContainer from '@components/Container'
import { useState } from 'react'
import FormComponents from './FormComponents'

export default function ReferralFormsComponent() {
  const [currentIndex, setCurrentIndex] = useState(1)
  const [formData, setFormData] = useState([])

  const timeLineLabels = [
    'Email',
    'Agent Details',
    'Service Type',
    'Accommodation Funding',
    'Community Participation',
    'Client Details'
  ]

  const handleBackClick = () => setCurrentIndex(ind => ind - 1)
  const handleNextClick = () =>
    setCurrentIndex(ind => {
      if (ind === timeLineLabels.length - 1) {
        // submit form
        console.log(formData)
        return ind + 1
      } else if (ind === timeLineLabels.length) return ind
      else return ind + 1
    })

  const handleDataChange = data => setFormData(data)

  return (
    <MainContainer>
      <div className='py-20 space-y-6'>
        <TextContent
          head='Client referal forms'
          para='Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
          Eu fermentum tortor sed sagittis, odio nibh ut. Maecenas dis tristique nunc, tortor. 
          Id at a massa egestas tristique tempus id ullamcorper sit. '
          center
        />
        <div className='border-2 border-gray-200 box-border grid grid-cols-1 md:grid-cols-8 lg:grid-cols-7'>
          <div className='col-span-3 lg:col-span-2 bg-darkgray-700 pt-8 md:p-8'>
            <TimeLine labels={timeLineLabels} currentIndex={currentIndex} />
          </div>
          <div className='col-span-5 bg-white flex flex-col justify-between'>
            <div className='p-10'>
              <FormComponents index={currentIndex} handleChange={handleDataChange} />
            </div>
            <div className='bg-gray-400 flex justify-between px-10 py-5 text-sm'>
              <button
                className={`uppercase p-3 px-4 ${
                  currentIndex !== 0 ? 'bg-darkgray-700 text-white' : 'bg-gray-300 text-gray-500 pointer-events-none'
                }`}
                onClick={handleBackClick}
              >
                Back
              </button>
              <button className={`uppercase p-3 px-4 text-white bg-yellow-500`} onClick={handleNextClick}>
                {currentIndex < timeLineLabels.length - 1 ? 'Next' : 'Submit'}
              </button>
            </div>
          </div>
        </div>
      </div>
    </MainContainer>
  )
}
