import InputField from '@components/common/InputField'
import { FC, useState } from 'react'

interface Props {
  index: number
  handleChange?: (data:any) => any
}

const FormComponents: FC<Props> = ({ index, handleChange }) => {
  const [formData, setFormData] = useState<any[]>([])

  const handleInputChange = (value, name) => {
    let _data = formData
    _data[index] = { ..._data[index], [name]: value }
    setFormData(_data);
    if(handleChange) handleChange(_data);
  }

  return (
    <div>
      {index === 0 && (
        <div>
          <p className='text-darkgray-700 text-3xl font-medium font-francois'>Email</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          <form className='mt-10 space-y-10'>
            <InputField placeholder='Email' onChange={val => handleInputChange(val, 'email')} />
          </form>
        </div>
      )}
      {index === 1 && (
        <div>
          <p className='text-darkgray-700 text-3xl font-medium font-francois'>Agent Details</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          <form className='mt-10 space-y-10'>
            <InputField placeholder='Your Full Name' onChange={val => handleInputChange(val, 'fullName')} />
            <InputField placeholder='Enter Organization' onChange={val => handleInputChange(val, 'organization')} />
            <InputField
              placeholder='Enter Contact Number'
              type='number'
              onChange={val => handleInputChange(val, 'contactNumber')}
            />
          </form>
        </div>
      )}
      {index === 2 && (
        <div>
          <p className='text-darkgray-700 text-3xl font-medium font-francois'>Service Type</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          <form className='mt-10 space-y-10'>
            <InputField placeholder='Your Full Name' onChange={val => handleInputChange(val, 'fullName')} />
            <InputField placeholder='Enter Organization' onChange={val => handleInputChange(val, 'organization')} />
            <InputField
              placeholder='Enter Contact Number'
              type='number'
              onChange={val => handleInputChange(val, 'contactNumber')}
            />
          </form>
        </div>
      )}
      {index === 3 && (
        <div>
          <p className='text-darkgray-700 text-3xl font-medium font-francois'>Accomodation Funding</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          <form className='mt-10 space-y-10'>
            <InputField placeholder='Your Full Name' onChange={val => handleInputChange(val, 'fullName')} />
            <InputField placeholder='Enter Organization' onChange={val => handleInputChange(val, 'organization')} />
            <InputField
              placeholder='Enter Contact Number'
              type='number'
              onChange={val => handleInputChange(val, 'contactNumber')}
            />
          </form>
        </div>
      )}
      {index === 4 && (
        <div>
          <p className='text-darkgray-700 text-3xl font-medium font-francois'>Community Participation</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          <form className='mt-10 space-y-10'>
            <InputField placeholder='Your Full Name' onChange={val => handleInputChange(val, 'fullName')} />
            <InputField placeholder='Enter Organization' onChange={val => handleInputChange(val, 'organization')} />
            <InputField
              placeholder='Enter Contact Number'
              type='number'
              onChange={val => handleInputChange(val, 'contactNumber')}
            />
          </form>
        </div>
      )}
      {index === 5 && (
        <div>
          <p className='text-darkgray-700 text-3xl font-medium font-francois'>Client Details</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          <form className='mt-10 space-y-10'>
            <InputField placeholder='Your Full Name' onChange={val => handleInputChange(val, 'fullName')} />
            <InputField placeholder='Enter Organization' onChange={val => handleInputChange(val, 'organization')} />
            <InputField
              placeholder='Enter Contact Number'
              type='number'
              onChange={val => handleInputChange(val, 'contactNumber')}
            />
          </form>
        </div>
      )}
    </div>
  )
}

export default FormComponents
