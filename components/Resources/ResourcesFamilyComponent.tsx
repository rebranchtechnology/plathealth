import Image from 'next/image'
import FamilyImage from '@assets/images/resources/family/playing.png'
import TextContent from '@components/common/TextContent'

export default function ResourcesFamilyComponent() {
  const paraList = [
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis elementum sed vel varius faucibus. Nunc mauris, purus consectetur nullam egestas. Elit, velit pretium, aliquam, tincidunt. Id aenean ipsum ipsum leo tortor, ut suspendisse. Tellus pharetra mi eget ullamcorper elit nibh neque. Vitae in cras eu morbi ut diam cursus.',
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis elementum sed vel varius faucibus. Nunc mauris, purus consectetur nullam egestas. Elit, velit pretium, aliquam, tincidunt. Id aenean ipsum ipsum leo tortor, ut suspendisse. Tellus pharetra mi eget ullamcorper elit nibh neque. Vitae in cras eu morbi ut diam cursus.'
  ]
  return (
    <div className='bg-bgGray flex justify-between items-stretch'>
      <div className='relative lg:w-1/2 h-auto hidden lg:block'>
        <Image src={FamilyImage} alt='Mission Image' layout='fill' objectFit='cover' />
      </div>
      <div className='w-11/12 m-auto lg:w-1/2 h-825 py-16 lg:py-20 xl:py-24 lg:px-16'>
        {' '}
        <TextContent head={'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'} para={paraList} />{' '}
      </div>{' '}
    </div>
  )
}
