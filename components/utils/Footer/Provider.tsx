import NdisLogo from '@assets/images/footer/NdisLogo.png'
import Map from '@assets/images/footer/map.png'
import Image from 'next/image'

export default function Provider() {
  return (
    <div className='bg-white py-10'>
      <div className='w-11/12 md:w-4/5 mx-auto flex gap-8 lg:gap-14 flex-col md:flex-row justify-between'>
        <div className='md:w-4/6 space-y-6'>
          <p className='text-4xl font-medium font-francois'>Registered NDIS Provider</p>
          <p>
            Medela Health is a registered NDIS provider that meets the high standards required by the NDIA. Whether your
            plan is self-managed, plan-managed or agency-managed, we can deliver supports that are right for you.
          </p>
          <Image src={NdisLogo} alt='ndis logo' />
        </div>
        <div className='sm:w-4/5 md:w-auto mx-auto'>
          <Image src={Map} alt='australia map' />
        </div>
      </div>
    </div>
  )
}
