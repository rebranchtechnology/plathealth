import Provider from './Provider'
import FAQs from '@components/FAQs'
import Logo from '@assets/images/Logo.png'
import NdisLogo from '@assets/images/footer/NdisLogo.png'
import Image from 'next/image'

const Footer = () => {
  return (
    <footer className='relative'>
      <Provider />
      <FAQs />

      <div className='bg-darkgray-900 bg-opacity-100 text-white'>
        <div className='w-full px-12 grid grid-cols-1 sm:grid-cols-2 xl:grid-cols-4 gap-4 gap-y-10 py-20 border-b-2 border-gray-300 flex-auto flex-col'>
          <div className='relative sm:col-span-1 col-span-4 space-y-4 text-base lg:text-xl mt-4 flex-col'>
            <div className='h-36 w-36'>
              {' '}
              <Image src={Logo} alt='Medela Health logo' />
            </div>
            <div className='h-36 w-36 pt-16'>
              {' '}
              <Image src={NdisLogo} alt='Ndis logo' />
            </div>
            <div>
              <p className='tracking-tight text-base font-light px-4 pt-4'>NDIS PROVIDER NUMBER: 4050017044</p>
            </div>
          </div>
          <div className='sm:col-span-1 col-span-4 space-y-4 text-base lg:text-xl'>
            <div></div>
            <p className='font-francois tracking-tight text-4xl'>About Medela Health</p>
            <p className='tracking-tight '>
              Medela Health is a registered NDIS provider offering a range of in-home care, community participation,
              daily living, and clinical care services across Australia.
            </p>
            <p>Medela Health</p>
            <p>ACN 618 833 417</p>
            {/* <a href='#' className='text-purple-600 hover:text-white'>
              Our Privacy Policy
            </a> */}
          </div>

          <div className='sm:col-span-1 col-span-4 space-y-4 text-base lg:text-xl pt-4'>
            <p className='font-francois tracking-tight text-4xl'>Get In Touch</p>
            <div className='space-y-2'>
              <p>
                Address:
                <a className='ml-2 hover:purple-600 tracking-tight'>56 Ellen Stirling Boulevard, Innaloo, WA 6018</a>
              </p>
              <p className='tracking-tight '>
                Call us today on{' '}
                <a className='font-extrabold text-md text-greenie hover:text-purple-600' href='tel:1300415409'>
                  1300 415 409
                </a>
                &nbsp; to speak to one of our friendly Care Advisers.
              </p>
              <div>
                <a className='text-greenie hover:text-purple-600' href='mailto:info@medelahealth.com.au'>
                  info@medelahealth.com.au
                </a>
              </div>
            </div>
          </div>

          <div className='col-span-4 sm:col-span-1 space-y-4 text-base lg:text-xl pt-4'>
            <p className='font-francois tracking-tight text-4xl'>Coronavirus (COVID-19) Vaccine information</p>
            <p>
              <a
                href='https://www.wa.gov.au/government/covid-19-coronavirus'
                className='text-greenie hover:text-purple-600'
              >
                COVID-19 information available here.
              </a>{' '}
            </p>
            <p>
              All of our staff are fully vaccinated and we have Covid-19 specific policies as a disability services
              provider.
            </p>
          </div>
        </div>
        <div className='sm:w-11/12 mx-auto pt-4 sm:py-4 flex space-y-4 sm:space-y-0 flex-col sm:flex-row justify-between items-center'>
          <span className='inline-flex items-baseline'>
            <p className='font-medium w-auto sm:w-full text-center sm:text-left mx-auto'>
              Copyright ©2022 Medela Health. All Rights Reserved. Built by
              <a className='text-greenie hover:text-purple-600' href='https://www.rebranch.com.au'>
                {' '}
                Rebranch
              </a>
            </p>{' '}
          </span>
        </div>
      </div>
    </footer>
  )
}

export default Footer
