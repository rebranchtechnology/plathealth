import Image from 'next/image'
import { FC } from 'react'

interface BannerProps {
  title: string
  description?: string
  image: StaticImageData
}

const Banner: FC<BannerProps> = ({ title, image, description }) => {
  return (
    <div className='relative bg-banner-bg1'>
      <div className='flex justify-end items-center flex-col md:flex-row'>
        <div className='w-full md:w-7/12 2xl:w-1/2 h-80 lg:h-96 bg-banner-bg2 bg-cover pl-10'>
          <div className='relative w-full min-h-full'>
            <Image src={image} layout='fill' className='object-fill' alt={`${title} banner image`} />
          </div>
        </div>
        <div className='md:absolute inset-0 h-full w-full flex items-center text-white'>
          <div className='w-4/5 mx-auto'>
            <div className='w-full md:w-1/2 lg:w-2/5 py-12 md:py-6 md:pr-4 space-y-4 text-center md:text-left'>
              <p className='text-5xl font-bold font-francois'>{title}</p>
              {description && <p className='text-xl'>{description}</p>}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Banner
