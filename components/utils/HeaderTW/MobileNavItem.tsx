import { Disclosure, Transition } from '@headlessui/react'

import { FC, Fragment } from 'react'

const DownArrow = ({ className }: { className?: string }) => (
  <svg className={className ?? ''} viewBox='0 0 12 12' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path
      d='M5.58793 9.2845C5.6338 9.35112 5.69518 9.40559 5.76678 9.44322C5.83837 9.48085 5.91805 9.50052 5.99893 9.50052C6.07982 9.50052 6.15949 9.48085 6.23109 9.44322C6.30268 9.40559 6.36406 9.35112 6.40993 9.2845L10.9099 2.7845C10.962 2.70953 10.9926 2.62172 10.9982 2.5306C11.0039 2.43949 10.9845 2.34856 10.9422 2.2677C10.8998 2.18683 10.8361 2.11912 10.7579 2.07193C10.6798 2.02473 10.5902 1.99986 10.4989 2H1.49893C1.40785 2.00038 1.3186 2.02557 1.24077 2.07288C1.16294 2.12019 1.09948 2.18782 1.05722 2.2685C1.01495 2.34917 0.995473 2.43985 1.00089 2.53077C1.0063 2.62168 1.03639 2.70941 1.08793 2.7845L5.58793 9.2845Z'
      fill='#3B3F3E'
    />
  </svg>
)

interface Props {
  name: string
  href: string
  navKey: string
  selectedNav: string
  subLinks?: {
    name: string
    href: string
  }[]
}

const MobileNavItem: FC<Props> = ({ name, href, navKey, selectedNav, subLinks }) => {
  return (
    <Disclosure as={Fragment}>
      {({ open }) => (
        <>
          <div className={`flex justify-between items-center text-black hover:text-gray-600 group text-lg font-medium`}>
            <a href={href} className={`p-3 flex-1 ${navKey === selectedNav && 'text-greenie'}`}>
              {name}
            </a>
            {subLinks && (
              <Disclosure.Button className='p-3 mr-1 rounded-sm md:rounded-lg bg-white bg-opacity-0 hover:bg-darkgray-800 hover:bg-opacity-20'>
                <DownArrow className={`w-4 h-4 ${open && 'transform rotate-180'}`} />
              </Disclosure.Button>
            )}
          </div>
          {subLinks && (
            <Transition
              show={open}
              as={Fragment}
              enter='transition-transform origin-top duration-100'
              enterFrom='transform scale-y-0'
              enterTo='transform scale-y-100'
              leave='transition-transform origin-top duration-100'
              leaveTo='transform scale-y-0'
            >
              <Disclosure.Panel className='px-6 flex flex-col md:rounded-lg font-normal bg-gray-200 divide-y divide-darkgray-400'>
                {subLinks.map(({ name, href }, i) => (
                  <a key={i} href={href} className={`p-2 px-4 flex-1 text-black hover:text-gray-600`}>
                    {name}
                  </a>
                ))}
              </Disclosure.Panel>
            </Transition>
          )}
        </>
      )}
    </Disclosure>
  )
}

export default MobileNavItem
