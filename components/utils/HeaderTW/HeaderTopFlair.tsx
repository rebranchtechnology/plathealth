export default function HeaderTopFlair() {
  return (
    <div className='bg-darkgray-700 text-white'>
      <div className='lg:w-11/12 mx-auto flex justify-between items-stretch'>
        <div className='hidden md:flex gap-2 lg:gap-4 items-center'>
          <div className='p-2 mt-auto mr-auto space-x-2'>
            <span>Phone:</span>
            <a className='font-bold' href='tel:1300415409'>
              1300 415 409
            </a>
          </div>
          <div className='p-2 mt-auto ml-auto space-x-2'>
            <span>Email:</span>
            <a className='font-bold' href='mailto:info@medelahealth.com.au'>
              info@medelahealth.com.au
            </a>
          </div>
        </div>
        <div className='w-full md:w-auto bg-greenie p-3 flex justify-center items-center space-x-2 text-sm'>
          <a href='tel:1300415409'>
            <b>CALL US ON</b> <span className='text-base'>1300 415 409</span>
          </a>
        </div>
      </div>
    </div>
  )
}
