/* This example requires Tailwind CSS v2.0+ */
import { Fragment, ReactNode } from 'react'
import { Popover, Transition } from '@headlessui/react'

import Image from 'next/image'
import Logo from '@assets/images/Logo.png'

import {
  BookmarkAltIcon,
  CalendarIcon,
  ChartBarIcon,
  CursorClickIcon,
  MenuIcon,
  PhoneIcon,
  PlayIcon,
  RefreshIcon,
  ShieldCheckIcon,
  SupportIcon,
  ViewGridIcon,
  XIcon
} from '@heroicons/react/outline'

import { ChevronDownIcon } from '@heroicons/react/solid'
import { pageRoutes } from '@lib/routes'
import { headerText } from '@lib/constants/headers'
import { navKeys } from '@lib/types'
import MobileNavItem from './MobileNavItem'
import HeaderTopFlair from './HeaderTopFlair'

const navLinks: { href: string; name: string; navKey: navKeys; subLinks?: any[] }[] = [
  {
    href: pageRoutes.home,
    name: headerText.home,
    navKey: 'home'
  },
  {
    href: pageRoutes.about,
    name: headerText.about,
    navKey: 'about'
  },
  {
    href: '#',
    name: headerText.services,
    navKey: 'services',
    subLinks: [
      {
        name: headerText.servicesDailyLiving,
        href: pageRoutes.servicesDailyLiving,
        description: 'Independence, your way.',
        icon: ShieldCheckIcon
      },
      {
        name: headerText.servicesAccommodation,
        href: pageRoutes.servicesAccommodation,
        description: 'A change of environment when you need it.',
        icon: ShieldCheckIcon
      },
      {
        name: headerText.servicesCommunity,
        href: pageRoutes.servicesCommunity,
        description: 'Driving your passions.',
        icon: ShieldCheckIcon
      },
      {
        name: headerText.servicesHousehold,
        href: pageRoutes.servicesHousehold,
        description: 'Tailored specifically to you.',
        icon: ShieldCheckIcon
      },
      {
        name: headerText.servicesCommunityNursing,
        href: pageRoutes.servicesCommunityNursing,
        description: 'Care in your own home.',
        icon: ShieldCheckIcon
      }
    ]
  },
  {
    href: pageRoutes.contact,
    name: headerText.contact,
    navKey: 'contact'
  }
]

function classNames(...classes: any) {
  return classes.filter(Boolean).join(' ')
}

export default function HeaderTW(props: { selectedNavKey: navKeys }) {
  const DesktopHref = (p: { name: string; href: string; navKey: navKeys }) => {
    return (
      <a
        href={p.href}
        className={`h-full block pt-1 text-base font-medium hover:text-gray-600 ${
          props.selectedNavKey === p.navKey ? 'text-greenie' : 'text-black'
        }`}
      >
        <span>{p.name}</span>
      </a>
    )
  }

  const DesktopListPop = (p: { name: string; list: any[]; children?: ReactNode; navKey: navKeys }) => {
    return (
      <Popover className='relative'>
        {({ open }) => (
          <>
            <Popover.Button
              className={classNames(
                open ? 'text-gray-600' : props.selectedNavKey === p.navKey ? 'text-greenie' : 'text-black',
                'h-full group rounded-md inline-flex items-center text-base font-medium hover:text-gray-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-greenie'
              )}
            >
              <span>{p.name}</span>
              <ChevronDownIcon
                className={classNames(open ? 'text-gray-600' : 'text-black', 'ml-2 h-5 w-5 group-hover:text-gray-600')}
                aria-hidden='true'
              />
            </Popover.Button>

            <Transition
              show={open}
              as={Fragment}
              enter='transition ease-out duration-200'
              enterFrom='opacity-0 translate-y-1'
              enterTo='opacity-100 translate-y-0'
              leave='transition ease-in duration-150'
              leaveFrom='opacity-100 translate-y-0'
              leaveTo='opacity-0 translate-y-1'
            >
              <Popover.Panel
                static
                className='absolute right-0 z-20 transform translate-x-10 mt-3 px-2 w-screen max-w-md sm:px-0'
              >
                <div className='rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 overflow-hidden'>
                  <div className='relative grid gap-6 bg-white px-5 py-8 sm:gap-8 sm:p-8'>
                    {p.list.map(item => (
                      <a
                        key={item.name}
                        href={item.href}
                        className='-m-3 p-3 flex items-start rounded-lg hover:bg-gray-50'
                      >
                        <item.icon className='flex-shrink-0 h-6 w-6 text-greenie' aria-hidden='true' />
                        <div className='ml-4'>
                          <p className='text-base font-medium text-gray-900'>{item.name}</p>
                          <p className='mt-1 text-sm text-gray-500'>{item.description}</p>
                        </div>
                      </a>
                    ))}
                  </div>
                  {p?.children ?? <></>}
                </div>
              </Popover.Panel>
            </Transition>
          </>
        )}
      </Popover>
    )
  }

  return (
    <>
      <HeaderTopFlair />
      <Popover className='font-francois shadow bg-white sticky top-0 z-50'>
        {({ open }) => (
          <>
            <div className='w-11/12 mx-auto flex flex-row lg:flex-col xl:flex-row justify-between items-center'>
              <div className='h-24 w-20 text-greenie bg-white p-2 rounded-md justify-center'>
                <a href={pageRoutes.home}>
                  <Image layout='responsive' src={Logo} alt='Medela Health logo' />
                </a>
              </div>

              <div className='lg:hidden'>
                <Popover.Button className='rounded-md p-2 inline-flex items-center justify-center text-black hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-greenie'>
                  <span className='sr-only'>Open menu</span>
                  {open ? (
                    <XIcon className='h-6 w-6' aria-hidden='true' />
                  ) : (
                    <MenuIcon className='h-6 w-6' aria-hidden='true' />
                  )}
                </Popover.Button>
              </div>

              <Popover.Group
                as='nav'
                className='hidden lg:flex self-center items-stretch space-x-5 xl:space-x-7 lg:pb-4 xl:pb-0'
              >
                {navLinks.map(({ href, name, navKey, subLinks }, i) =>
                  !subLinks ? (
                    <DesktopHref key={i} href={href} name={name} navKey={navKey} />
                  ) : (
                    <DesktopListPop key={i} name={name} list={subLinks} navKey={navKey} />
                  )
                )}
              </Popover.Group>
            </div>

            <Transition
              show={open}
              as={Fragment}
              enter='duration-200 ease-out'
              enterFrom='opacity-0 scale-95'
              enterTo='opacity-100 scale-100'
              leave='duration-100 ease-in'
              leaveFrom='opacity-100 scale-100'
              leaveTo='opacity-0 scale-95'
            >
              <Popover.Panel
                static
                className='absolute inset-x-0 z-20 md:p-2 transition transform origin-top-right lg:hidden'
              >
                <div className='md:rounded-lg md:shadow-lg md:ring-1 ring-black ring-opacity-5 bg-white divide-y-2 divide-gray-50'>
                  <div className='px-4'>
                    <nav className='divide-y-2'>
                      {navLinks.map(({ name, navKey, href, subLinks }, i) => (
                        <MobileNavItem
                          key={i}
                          name={name}
                          navKey={navKey}
                          href={href}
                          subLinks={subLinks}
                          selectedNav={props.selectedNavKey}
                        />
                      ))}
                    </nav>
                  </div>
                </div>
              </Popover.Panel>
            </Transition>
          </>
        )}
      </Popover>
    </>
  )
}
