import StarImg from '@assets/images/Star.png'
import PuzzleImg from '@assets/images/Puzzle.png'
import Image from 'next/image'
import TextContent from '@components/common/TextContent'

import missionImg from '@assets/images/home/Mission.png'
import { PlusCircleIcon } from '@heroicons/react/outline'

const facts = [
  {
    text: 'Trustworthy, experienced carers',
    img: StarImg
  },
  {
    text: 'Translation services available',
    img: StarImg
  },
  {
    text: 'Holistic care to address all your requirements',
    img: StarImg
  },
  {
    text: 'All cultures and backgrounds respected',
    img: StarImg
  }
]

export default function OurMission() {
  return (

    <div className='bg-white flex justify-between items-stretch'>
      <div className='relative lg:w-1/2 h-auto hidden lg:block'>
        <Image src={missionImg} alt='Mission Image' layout='fill' objectFit='cover' />
      </div>

      <div className='w-11/12 m-auto lg:w-1/2 h-825 py-16 lg:py-20 xl:py-24 lg:px-16'>
        <TextContent
          head={'Our Mission is to help people of all abilities to live safely, actively and with joy in their own homes and communities'}
          para={'We deliver services in line with our core values of inclusivity and respect. You choose how we support you according to your needs.'} />

        {facts.map((fact, idx) => (
          <div key={idx} className='mx-auto'>
            <ul className='list-inside ...'>
              <li className='mt-4 flex gap-4 items-center'>
                <PlusCircleIcon className='h-12 w-12 text-purpie'/>
                <p className='font-bold flex-1'>
                  {fact.text}
                </p>
              </li>
            </ul>
          </div>
        ))}
      </div>
    </div>
  )
}
