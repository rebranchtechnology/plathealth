import MainContainer from '@components/Container'
import MainContent from '@components/Container/MainContent'
import { HeartIcon } from '@heroicons/react/outline'
import { HomeIcon } from '@heroicons/react/outline'
import { UserGroupIcon } from '@heroicons/react/outline'
import { ClipboardCheckIcon } from '@heroicons/react/outline'
import { pageRoutes } from '@lib/routes'
import ServiceCard from './ServiceCard'
import { FC } from 'react'

const services = [
  {
    name: 'Assistance with daily life tasks in a group or shared living arrangement',
    description:
      'We focus on assisting our participants in developing and nurturing their domestic and life skills to enhance their independence. We deliver these services in shared accommodation or in purpose-built homes, taking care to always ensure the privacy and personal space of participants. In this arrangement, we provide one-on-one support and assist with daily tasks.',
    icon: UserGroupIcon,
    learnMoreLink: pageRoutes.servicesDailyLiving
  },
  {
    name: 'Short & Medium Term Accomodation or Respite',
    description:
      'We provide short-term accomodation to expose our clients to different environments where they can still access the same level of support. Medium Term Accommodation is offered to participants who require temporary housing while they transition to their permanent homes or longer term arrangements.',
    icon: HeartIcon,
    learnMoreLink: pageRoutes.servicesAccommodation
  },
  {
    name: 'Participation in Community, Social and Civic Activities',
    description:
      'At Medela Health, we assist our participants by helping them participate in the community and facilitating their engagement in social activities. We believe that social connection and community engagement are crucial aspects of daily life, we provide a variety of one-on-one community access, community outreach activities and community group activities for people with disabilities, according to their goals and passions.',
    icon: HomeIcon,
    learnMoreLink: pageRoutes.servicesCommunity
  },
  {
    name: 'Assistance with Daily Personal Activity',
    description:
      "Our assistance with daily personal care activities is tailored around the individual and their needs. These personal care services are adaptable, and custom built to the individual's specific needs and objectives. We not only assist our participants with their chores, but we also assist them in developing their independence.",
    icon: ClipboardCheckIcon,
    learnMoreLink: pageRoutes.servicesHousehold
  },
  {
    name: 'Community Nursing',
    description:
      "The Medela Health team is caring, friendly, professional, and compliant with all NDIS Practice Standards. We are committed to ensuring you remain in control of the nursing services we deliver. Whether it's overnight care, respite or ongoing health management, our highly-credentialed Community Nurses are culturally sensitive and can liaise with your regular GP and other health providers, so you can be sure your needs are being met by a trusted team.",
    icon: HeartIcon,
    learnMoreLink: pageRoutes.servicesCommunityNursing
  },
  {
    name: 'Specialist Disability Accomodation',
    description:
      'There is a lack of accessible housing for people with disabilities. We work with a number of property and building groups to find accommodation to enable our clients to live as independently as possible. We involve clients, their guardians and their extended support networks in choosing where they would like to live and delivering NDIS certified accomodation designed by NDIA accreddited architects and developers.',
    icon: HeartIcon,
    learnMoreLink: pageRoutes.servicesAccommodation
  }
]

const ServicesComponent: FC = () => {
  return (
    <MainContainer>
      <div className='lg:text-center lg:pt-36 xl:pt-72 2xl:pt-96'>
        <h2 className='text-4xl text-greenie font-semibold tracking-wide uppercase mb-4 xl:pt-12 2xl:pt-48 pt-8'>
          Services
        </h2>
        <p className='mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl'>
          Quality care, delivered by quality teams
        </p>
        <p className='mt-4 max-w-2xl text-xl text-gray-500 lg:mx-auto'>
          At Medela Health, care comes first. We believe in personalising our care services to the needs of the people
          we look after and involve them and their support networks in meeting their goals, growing their passions, and
          enabling them to live as independently, and comfortablty as possible.
        </p>
      </div>

      <div className='mt-10 w-full h-full'>
        <div className='grid grid-cols-1 lg:grid-cols-2 gap-4'>
          {services.map(({ icon, name, description, learnMoreLink }, i) => (
            <ServiceCard key={i} Icon={icon} name={name} description={description} learnMoreLink={learnMoreLink} />
          ))}
        </div>
      </div>
      <div>
        <p className='flex justify-center mt-8 text-xl leading-8 font-semi-bold tracking-tight text-gray-900 sm:text-3xl'>
          Are you currently searching for a reliable service provider?
        </p>
        <p className='mt-4 max-w-2xl text-xl text-gray-500 lg:mx-auto'>
          Our disability support workers are highly trained and experienced in dealing with a wide range of complex
          cases, they are the most understanding, caring, nurturing individuals that you will ever come across. They are
          also accredited and certified with a strong understanding of disabilities. We do annual mental health
          training, first aid certification checks, manual handling training, working with children checks, and police
          clearance checks. You can be assured that we will go above and beyond to find the right mix of support to
          enable individuals and their families to achieve their goals no matter how big or small. Reach out today to
          get in touch with our team.
        </p>
        <div className='text-center pt-4'>
          {' '}
          <button
            type='button'
            className='uppercase bg-purpie text-white py-2 lg:w-1/4 w-full md:w-1/2 rounded-sm hover:bg-greenie'
          >
            <a href={pageRoutes.contact}>Contact Us</a>
          </button>
        </div>
      </div>
    </MainContainer>
  )
}

export default ServicesComponent
