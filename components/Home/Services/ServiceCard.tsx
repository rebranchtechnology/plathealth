import Image from 'next/image';
import { FC, ReactElement } from 'react';

interface Props {
  Icon: FC;
  name: string;
  description: string;
  learnMoreLink: string;
}

const ServiceCard: FC<Props> = ({
  Icon,
  name,
  description,
  learnMoreLink,
}) => {
  return (
    <div className='group bg-greenie text-center border-2 border-gray-200 rounded-md p-4 pb-8 flex justify-between space-y-4 flex-col hover:bg-purpie'>
      <div className='space-y-4'>
        <div className='w-max mx-auto text-white group-hover:text-white'>
          <Icon />
        </div>
        <div className='space-y-4'>
          <p className='text-2xl font-bold text-white group-hover:text-mainBlue'>
            {name}
          </p>
          <p className='text-white leading-tight group-hover:text-white'>
            {description}
          </p>
        </div>
      </div>
      <a
        href={learnMoreLink}
        className='block w-max mx-auto uppercase py-4 px-8 bg-white hover:bg-greenie hover:text-white text-purpie text-sm font-medium rounded-sm'
      >
        Learn More
      </a>
    </div>
  );
};

export default ServiceCard;
