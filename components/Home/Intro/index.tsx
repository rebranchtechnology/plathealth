import { pageRoutes } from '@lib/routes'
import Image from 'next/image'
import HomeBackground from '@assets/images/HomeBackground.png'

export default function HomeIntro() {
  return (
    <div className='relative xl:pb-48 md:pb-48 2xl:pb-96'>
      <div className='absolute inset-0'>
        <div className='w-full h-full mb-48'>
          <Image src={HomeBackground} alt='Medela Health' />
        </div>
      </div>
      <div className='relative mx-auto max-w-7xl w-full h-auto pt-16 pb-20 text-center lg:py-24 lg:text-left'>
        <div className='bg-purpie bg-opacity-60 p-4 lg:w-1/2 sm:px-8 xl:pr-16'>
          <h1 className='text-4xl tracking-tight font-extrabold text-white sm:text-4xl'>We Care</h1>
          <p className='mt-3 max-w-md mx-auto text-lg text-white sm:text-xl md:mt-5 md:max-w-3xl'>
            At Medela Health, we&apos;re all about friendly and inclusive support for people of all abilities...
          </p>
        </div>
      </div>
    </div>
  )
}
