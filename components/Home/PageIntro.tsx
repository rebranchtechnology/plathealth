import { FC } from 'react'

import MainContainer from '@components/Container'
import img1 from '@assets/images/home/Rectangle 53.png'
import img2 from '@assets/images/home/Rectangle 54.png'
import img3 from '@assets/images/home/Rectangle 55.png'
import CardText from '@components/common/CardText'
import FlexWrapper from '@components/Container/FlexWrapper'

const PageIntro: FC<{}> = () => {

    type cardElements = {
        img: StaticImageData;
        title: string;
        para: string;
    }

    const cardElements = [
        {
            img: img1,
            title: 'Western Australia-Wide Services',
            para: 'No matter where you\'re located, we have a team of disability carers local to your area.',
        },
        {
            img: img2,
            title: 'Wide Range Of Care',
            para: 'From in-home care, transport and community nursing, we have your needs covered.',
        },
        {
            img: img3,
            title: 'Around The Clock Support',
            para: 'Our team of carers and nurses are available any time you need them, 24/7.',
        },
    ]

    return (
        <MainContainer>
            <h1 className='text-center mb-12 text-greenie'>We’re here for you whenever and wherever you need us</h1>
            <FlexWrapper>
                {cardElements.map( (item, idx) => <CardText key={idx} img={item.img} title={item.title} para={item.para} color={'greenie'}/>)}
            </FlexWrapper>
            
        </MainContainer>
    )
}

export default PageIntro
