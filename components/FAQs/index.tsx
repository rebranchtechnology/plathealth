import MainContainer from '@components/Container'
import Image from 'next/image'
import { pageRoutes } from '@lib/routes'
import { Disclosure } from '@headlessui/react'
import { ChevronDownIcon } from '@heroicons/react/outline'

const faqs = [
  {
    question: 'Who can Medela Health aid with disability support services in Western Australia?',
    answer:
      'At Medela Health, we are available 24 hours a day 7 days a week, which makes us the ideal choice for all your support needs, no matter when, what time or where you find yourself needing support. We understand that life has plenty of twists and turns, and that is why we are here and available at any given time. Our disability support services across the state are available to all age ranges and these support services are delivered by our team of highly trained and experienced staff members. Our staff are specialized in operating and working with challenging and complex behaviors. At Medela Health, we honor the abilities, courage, and diversity of every individual living with a disability and endeavor to provide services that are unique and tailored to the individual and their support networks.'
  },
  {
    question: 'Why should I choose Medela Health?',
    answer:
      'Our disability support workers are recruited through a rigorous process, which involves a minimum - Certificate III in Disability, Working with Children Check, Police Clearance, First Aid Certification, Manual Handling Certification, induction and testing of understanding our policies and procedures, and a post induction brief on the profile and needs of participants they will work with. Our team works tirelessly on assisting you in developing the skills you need to develop community connections and access the support services that will enable you to form and sustain the best living conditions. If you think you or someone you know will benefit from our disability support services in Western Australia, please do not hesitate to get in touch with us today. We are ready and able to assist with all situations.'
  },
  {
    question: 'What NDIS services does Medela Health offer?',
    answer:
      'We offer assitance with daily living tasks in a group or shared living arrangement, community participation, assisstance'
  },
  {
    question: 'If I or a participant I support needs accomodation can Medela Health help?',
    answer:
      'Yes, Medela Health can lease homes on behalf of participants. We are also developing Specialist Disability Accomodation (SDA) with our partners to better support existing and future clients.'
  },
  {
    question: 'What Is The National Disability Insurance Scheme?',
    answer:
      'By now most people are familiar with the National Disability Insurance Scheme (NDIS). However, we are aware that there still some individuals and stakeholders who are not well acquainted with it. NDIS is a scheme that was implemented by the NDIA, or the National Disability Insurance Agency. The scheme was released nation-wide instead of different states and territories funding schemes. The scheme is centered around funding being directly provided to individuals, with permanent and significant disability, whilst also providing them with the independence and freedom of choice as to how their support needs are serviced. The NDIS ultimately is there to provide Australians living with disabilities the information they require, freedom of choice to match their goals and objections and also connecting them to services in their communities. This support system aids the benefactors to: 1) Freely decide on who/how their support services are delivered 2) Pursue and reach their goals 3) Be more informed about the services and support systems around them 4) Aid them in participating in the community'
  }
]

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

export default function FAQs() {
  return (
    <>
      <MainContainer>
        <div className='max-w-3xl mx-auto divide-y-2 divide-gray-200 mt-4 mb-8'>
          <h2 className='text-center text-3xl font-extrabold text-gray-900 sm:text-4xl'>Frequently asked questions</h2>
          <dl className='mt-6 space-y-6 divide-y divide-gray-200'>
            {faqs.map(faq => (
              <Disclosure as='div' key={faq.question} className='pt-6'>
                {({ open }) => (
                  <>
                    <dt className='text-lg'>
                      <Disclosure.Button className='text-left w-full flex justify-between items-start text-gray-400'>
                        <span className='font-medium text-gray-900'>{faq.question}</span>
                        <span className='ml-6 h-7 flex items-center'>
                          <ChevronDownIcon
                            className={classNames(open ? '-rotate-180' : 'rotate-0', 'h-6 w-6 transform')}
                            aria-hidden='true'
                          />
                        </span>
                      </Disclosure.Button>
                    </dt>
                    <Disclosure.Panel as='dd' className='mt-2 pr-12'>
                      <p className='text-base text-gray-500'>{faq.answer}</p>
                    </Disclosure.Panel>
                  </>
                )}
              </Disclosure>
            ))}
          </dl>
        </div>
      </MainContainer>
      <div className='relative top-o left-0 w-full overflow-hidden leading-none transform rotate-180 bg-white'>
        <svg
          className='relative block w-full h-1rem'
          data-name='Layer 1'
          xmlns='http://www.w3.org/2000/svg'
          viewBox='0 0 1200 120'
          preserveAspectRatio='none'
          fill='#171717'
        >
          <path d='M1200 120L0 16.48 0 0 1200 0 1200 120z' className='shape-fill'></path>
        </svg>
      </div>
    </>
  )
}
