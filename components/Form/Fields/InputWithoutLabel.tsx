import { FC } from 'react'

interface Props {
  type: 'text' | 'email' | 'password'
  label?: string
  name?: string
  placeholder?: string
  registerFunction: any
}

const InputWithoutLabel: FC<Props> = ({ name, label, placeholder, type, registerFunction }) => {
  return (
    <div>
      <label htmlFor={`${type}`} className='sr-only'>
        {label || type}
      </label>
      <input
        {...registerFunction}
        type={`${type}`}
        name={`${name || label}`}
        id={`${label || type}`}
        className='py-3 px-5 shadow-sm focus:ring-gray-600 focus:border-gray-600 block w-full sm:text-sm border border-gray-300 rounded-sm'
        placeholder={`${placeholder || ''}`}
      />
    </div>
  )
}

export default InputWithoutLabel
