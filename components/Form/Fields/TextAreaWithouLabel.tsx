import { FC } from 'react'

interface Props {
  label?: string
  name: string
  placeholder?: string
  registerFunction?: any
}

const TextAreaWithoutLabel: FC<Props> = ({ label, name, placeholder, registerFunction }) => {
  return (
    <div>
      <label htmlFor={`${name}`} className='sr-only'>
        {label || name}
      </label>
      <textarea
      {...registerFunction}
        className='py-3 px-5 shadow-sm focus:ring-gray-600 focus:border-gray-600 block w-full sm:text-sm border border-gray-300 rounded-sm resize-y'
        id={`${label || name}`}
        name={`${name || label}`}
        placeholder={`${placeholder || ''}`}
        rows={8}
      />
    </div>
  )
}

export default TextAreaWithoutLabel
