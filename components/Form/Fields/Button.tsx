import { FC } from 'react'

interface Props {
  name: string
}

const Button: FC<Props> = ({ name }) => {
  return <div className='py-1 bg-yellie text-white flex justify-center align-center w-full'>{name}</div>
}

export default Button
