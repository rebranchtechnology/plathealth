import Banner from '@components/utils/Banner'
import PageIntro from '@components/Home/PageIntro'
import BaseLayout from '@layouts/Base'
import { pageRoutes } from '@lib/routes'
import OurMission from '@components/Home/OurMission'
import OurServicesIntro from '@components/Home/Services/ServicesComponent'
import BodyContainer from '@components/Container/BodyContainer'
import HomeIntro from '@components/Home/Intro'

export default function Home() {
  return (
    <BaseLayout
      title='Medela Health'
      selectedNavKey='home'
      description='Medela Health core NDIS supports'
      canonical={pageRoutes.home}
    >
      <HomeIntro />
      <BodyContainer>
        <OurServicesIntro />
        <PageIntro />
        <OurMission />
      </BodyContainer>
    </BaseLayout>
  )
}
