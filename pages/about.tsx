import BaseLayout from '@layouts/Base'
import { pageRoutes } from '@lib/routes'

import About from '@components/About'
import OurMission from '@components/Home/OurMission'

export default function AboutPage() {
  return (
    <BaseLayout title='About' selectedNavKey='about' description='About Medela Health...' canonical={pageRoutes.about}>
      <About />
      <OurMission />
    </BaseLayout>
  )
}
