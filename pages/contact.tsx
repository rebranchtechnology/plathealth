import Banner from '@components/utils/Banner'
import BaseLayout from '../layouts/Base'

import { pageRoutes } from '../lib/routes'
import BannerImage from '@assets/images/contact/banner.png'
import ContactComponent from '@components/Contact/ContactComponent'

export default function ContactPage() {
  return (
    <BaseLayout
      title='Contact'
      selectedNavKey='contact'
      description='Contact Medela Health'
      canonical={pageRoutes.contact}
    >
      {' '}
      <ContactComponent />
    </BaseLayout>
  )
}
