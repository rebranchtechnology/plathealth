import BaseLayout from '@layouts/Base'
import { pageRoutes } from '@lib/routes'
import Accommodation from '@components/Services/accommodation'
export default function AccommodationPage() {
  return (
    <BaseLayout
      title='Accommodation'
      selectedNavKey='services'
      description='Medela Health Accommodation'
      canonical={pageRoutes.servicesAccommodation}
    >
      <Accommodation />
    </BaseLayout>
  )
}
