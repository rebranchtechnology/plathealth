import React, { FC } from 'react'

import BaseLayout from '@layouts/Base'
import BodyContainer from '@components/Container/BodyContainer'
import MainContainer from '@components/Container'
import TextContent from '@components/common/TextContent'

import { pageRoutes } from '@lib/routes'

import Image from 'next/image'
import CardImage from '@components/common/CardImage'
import img1 from '@assets/images/coreSupport/Rectangle 58.png'
import img2 from '@assets/images/coreSupport/Rectangle 59.png'
import img3 from '@assets/images/coreSupport/Rectangle 60.png'
import supportImg from '@assets/images/coreSupport/Rectangle 56.png'
import FlexWrapper from '@components/Container/FlexWrapper'
import Banner from '@components/utils/Banner'
import img from '@assets/images/coreSupport/Vector 1.png'
import Services from '@components/Services/index'

const index: FC<{}> = () => {
  const paraList = 'Core Support Services help you with everyday activities to get the most out of life'

  const services = [
    {
      img: img1,
      title: 'Daily Household Tasks',
      link: '/services/household-tasks'
    },
    {
      img: img2,
      title: 'Community Participation',
      link: '/services/community-participation'
    },
    {
      img: img3,
      title: 'Supported Independent Living',
      link: '/services/supported-independent'
    }
  ]

  return (
    <BaseLayout
      title='Medela Health'
      selectedNavKey='services'
      description='Medela Health'
      canonical={pageRoutes.services}
    >
      <Banner title={'Services'} image={img} />
      <BodyContainer>
        <Services />
      </BodyContainer>
    </BaseLayout>
  )
}

export default index
