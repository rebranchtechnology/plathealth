import React, { FC } from 'react'

import BaseLayout from '@layouts/Base'

import { pageRoutes } from '@lib/routes'

import HouseholdTasks from '@components/Services/householdTasks'

export default function DailyLivingPage() {
  return (
    <BaseLayout
      title='Daily Household Tasks'
      selectedNavKey='services'
      description='Help with daily tasks'
      canonical={pageRoutes.servicesDailyLiving}
    >
      <HouseholdTasks />
    </BaseLayout>
  )
}
