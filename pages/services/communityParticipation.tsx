import React, { FC } from 'react'

import BaseLayout from '@layouts/Base'

import { pageRoutes } from '@lib/routes'

import CommunityParticipation from '@components/Services/communityParticipation'

export default function CommunityParticipationPage() {
  return (
    <BaseLayout
      title='Community Participation'
      selectedNavKey='services'
      description='Medela Health drives your passions through community participation.'
      canonical={pageRoutes.servicesCommunity}
    >
      <CommunityParticipation />
    </BaseLayout>
  )
}
