import BaseLayout from '@layouts/Base'
import { pageRoutes } from '@lib/routes'
import Nursing from '@components/Services/nursing'
export default function CommuntiyNursingPage() {
  return (
    <BaseLayout
      title='Community Nursing'
      selectedNavKey='services'
      description='Medela Health community nursing'
      canonical={pageRoutes.servicesCommunityNursing}
    >
      <Nursing />
    </BaseLayout>
  )
}
