import React, { FC } from 'react'

import BaseLayout from '@layouts/Base'
import BodyContainer from '@components/Container/BodyContainer'
import MainContainer from '@components/Container'
import TextContent from '@components/common/TextContent'

import { pageRoutes } from '@lib/routes'

import DailyLiving from '@components/Services/dailyLiving'

export default function DailyLivingPage() {
  return (
    <BaseLayout
      title='Daily Living'
      selectedNavKey='services'
      description='Assistance with daily living or supported independence in a group or shared arrangement'
      canonical={pageRoutes.servicesDailyLiving}
    >
      <DailyLiving />
    </BaseLayout>
  )
}
