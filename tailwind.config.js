const colors = require('tailwindcss/colors')

module.exports = {
  important: true,
  purge: [
    './components/**/*.tsx',
    './components/**/**/*.tsx',
    './components/**/**/**/*.tsx',
    './pages/**/*.tsx',
    './pages/*.tsx',
    './layouts/**/*.tsx'
  ],

  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        transparent: 'transparent',
        greenie: '#97BFB2',
        bluie: '#0b629c',
        cardBlue: '#00C2FF',
        yellie: '#D1C006',
        lBlack: '#6B6B6B',
        mBlack: '#3A3D3B',
        greenTag: '#97BFB2',
        purpie: '#42176A',
        pinkie: '#C24A5C',
        bgGray: '#E5E5E5',
        darkgray: colors.trueGray,
        yellow: colors.yellow,
        green: colors.green
      },
      borderRadius: {
        arc: '50% 40%'
      },
      fontFamily: {
        francois: ['Francois One', 'sans-serif'],
        raleway: ['Raleway', 'sans-serif']
      },
      fontWeight: {
        extrabold: 800,
        'extra-bold': 800,
        black: 800
      },
      backgroundColor: theme => ({
        bgGray: '#EFF1F3'
      }),
      backgroundImage: theme => ({
        'landing-pattern': "url('./assets/images/RectangleBg.png')",
        'landing-pattern-2': "url('./assets/images/RectangleBg2.png')",
        'footer-pattern': "url('./assets/images/FooterBg.png')",
        'footer-pattern-2': "url('./assets/images/FooterBg2.png')",
        'sil-pattern': "url('./assets/images/SilBg.png')",
        'about-background': "url('./assets/images/about/About.png')",
        'sil-background': "url('./assets/images/SilBg.png')",
        'community-background': "url('./assets/images/CommunityBackground.png')",
        'daily-background': "url('./assets/images/dailyhousehold/DailyHousehold.png')",
        'news-blogs': "url('./assets/images/news/news-blogs.png')",
        'banner-bg1': "url('./assets/images/banner/bg.svg')",
        'banner-bg2': "url('./assets/images/banner/bg2.svg')"
      }),
      outline: {
        black: ['2px solid #c7c7c7', '1px']
      },
      spacing: {
        '32%': '32%',
        '48%': '48%'
      }
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
}
